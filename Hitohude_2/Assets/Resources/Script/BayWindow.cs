﻿using UnityEngine;
using System.Collections;

public class BayWindow : MonoBehaviour {

    //Canvas canvas_;

    public enum SHOW_STATE
    {
        STATE_NONE = 0,
        STATE_HINT,
        STATE_ANSWER
    }

    public enum BAY_STATE
    {
        STATE_NONE = 0,
        STATE_HINT_BAY,
        STATE_ANSWER_BAY,
        STATE_NO_BAY,
    }

    public static SHOW_STATE is_show;
    public static BAY_STATE is_bay;

    float dest_x = 200.0f;
    float first_x = 1022.0f;

	// Use this for initialization
	void Start () {
        is_bay = BAY_STATE.STATE_NONE;
        is_show = SHOW_STATE.STATE_NONE;
        Vector3 pos = transform.position;
        pos.x = first_x;
        transform.position = pos;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        if (is_show != SHOW_STATE.STATE_NONE)
        {
            pos.x += (dest_x - pos.x) * 0.1f;
        }
        else {
            pos.x += (first_x - pos.x) * 0.1f;
        }
        transform.position = pos;

    }

    static public void ResetBayState()
    {
        is_bay = BAY_STATE.STATE_NONE;
        is_show = SHOW_STATE.STATE_NONE;
    
    }
}
