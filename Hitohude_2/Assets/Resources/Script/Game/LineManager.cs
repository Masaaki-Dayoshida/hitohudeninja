﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineManager : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AllSolidRelease()
    {
		//シーン上に存在する実体線を全て探しだす
       SolidLine[] solidLine =  GameObject.FindObjectsOfType<SolidLine>();
       int num = solidLine.GetLength(0);
        int cnt = 0;
        while (cnt < num)
        {
            if (solidLine[cnt] == null)
            {
                break;
            }
			//見つけた線を全部リリース
            solidLine[cnt].Release();
            cnt++;
        }
		//カーソルの状態をリセット
        GameObject.FindObjectOfType<Cursor>().SetActive(false);
		//答えが表示されていれば、それもリセット
		GameObject.FindObjectOfType<ListManager>().ResetAnswer();

		LinePoint[] linePoint = GameObject.FindObjectsOfType<LinePoint>();

		//念のためにフラグを全部リセット
		foreach (var point in linePoint)
		{
			List<bool> list = point.GetFlagList();
			for (int count = 0; count < list.Count; ++count)
			{
				if (list[count])
				{
					list[count] = false;
				}
			}
			
		}
    }
}
