﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using GoodPlaceFramework;
//using GoodPlaceFramework;
using System.Security.Cryptography;
public class SNSManager : MonoBehaviour
{
	public static SNSManager instance = null;
	public static bool isPostBoost = false;
	void Awake(){
		if ( instance != null ){
			Destroy(gameObject);
			return;
		}
		instance = this;
        Debug.Log("Create SNSManager");
        DontDestroyOnLoad(gameObject);
	}
	public void OnPostFacebook(string status){
		if ( status == "OK" ){
			Debug.Log("OnPostFacebook OK");
			if ( isPostBoost ){
                int chip = int.Parse(Server.GetBind("shareChip"));
                SavedataManager.ValuePoint(chip);
                DialogManager.instance.AddChip(chip, chip + " ポイント  獲得しました!");
            }
		}else if ( status == "Cancel" ){
			Debug.Log("OnPostFacebook Cancel");
		}else{
			Debug.Log("OnPostFacebook NotFound");
		}
		isPostBoost = false;

	}
	public void OnPostTwitter(string status){
        if (status == "OK")
        {
            Debug.Log("OnPostTwitter OK");
			if ( isPostBoost ){
                int chip = int.Parse(Server.GetBind("shareChip"));
                SavedataManager.ValuePoint(chip);
                DialogManager.instance.AddChip(chip, chip + " ポイント  獲得しました!");
			}
		}else if ( status == "Cancel" ){
			Debug.Log("OnPostTwitter Cancel");
		}else{
			Debug.Log("OnPostTwitter NotFound");
		}
		isPostBoost = false;

	}
}
