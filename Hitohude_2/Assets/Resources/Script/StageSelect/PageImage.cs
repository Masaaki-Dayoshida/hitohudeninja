﻿using UnityEngine;
using System.Collections;

public class PageImage : MonoBehaviour {

    public string page_num = "01";
    public int target_stage = 0;

	// Use this for initialization
	void Start () {
        if (SavedataManager.ClearStageNum() > target_stage)
        {
            SpriteRenderer sprite_render = GetComponent<SpriteRenderer>();
            sprite_render.color = Color.white;
            sprite_render.sprite = ServerManager.DownloadSprite("Texture/StageSelect/page" + page_num + ".png");
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
