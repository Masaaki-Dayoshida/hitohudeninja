﻿using UnityEngine;
using System.Collections;
using System.IO;

public class FaceBookShare : MonoBehaviour
{
    public void OnClick()
    {
        File.WriteAllBytes(Application.persistentDataPath + "/screenshot.png", Resources.Load<Texture2D>("Texture/Title/title_bg").EncodeToPNG());

        Intent.OnPostToFacebook(
        "ポイント加算!",
        "https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw",
        "" + Application.persistentDataPath + "/screenshot.png");

    }
}
