﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class numberManager : MonoBehaviour {
	public GameObject number;

	private List<GameObject> numberObject = new List<GameObject>();
	private int CountPlace = 0;
	private uint value = 0;

	const float offsetX = 0.45f;
	// Use this for initialization
	void Start () {
		uint buff = value;
		numberObject.Clear();
		Vector3 pos = Vector3.zero;
		pos.x -= (CountPlace/2)*offsetX;
		if(CountPlace%2==0)
		{
			pos.x += offsetX/2.0f;
		}
		for (uint cnt = 0; cnt < CountPlace; cnt++)
		{
			GameObject obj = Instantiate(number, pos, Quaternion.identity) as GameObject;
			pos = obj.transform.position;
			obj.transform.parent = gameObject.transform;
			obj.transform.localPosition = pos;
			obj.renderer.material.SetTextureOffset("_MainTex",new Vector2(0.1f * (buff % 10), 0));
			numberObject.Add(obj);
			buff /= 10;
			pos.x -= offsetX;

		}
	}

	// Update is called once per frame
	void Update()
	{
	
	}

	public void SetNum(uint num)
	{
		uint buff = num;
		while (true)
		{
			buff /= 10;
			CountPlace++;
			if (buff <= 0)
			{
				break;
			}
		}

		value = num;

	}
}
