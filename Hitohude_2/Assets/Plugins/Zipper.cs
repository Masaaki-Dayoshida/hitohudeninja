﻿using Ionic.Zip;
using Ionic.Zlib;
using System.Text;
using System;
using System.IO;
using UnityEngine;
using GoodPlaceFramework;
#if UNITY_EDITOR
using UnityEditor;
#endif
public static class Zipper{
	const int FILESIZE = 1024 * 30; // 分割サイズ.
	public static void SliceZip(string name){
		#if UNITY_EDITOR
		byte[] src = File.ReadAllBytes("./Build/"+name+".zip");
		int num = 0;
		for (int remain = src.Length; remain > 0; remain -= FILESIZE) {

			// 作成する分割ファイルの実際のサイズ.
			int length = Math.Min(FILESIZE, remain);

			// 分割ファイルへ書き出すbyte配列の作成.
			byte[] dest = new byte[length];
			Array.Copy(src, num * FILESIZE, dest, 0, length);

			// 出力ファイル名（{ファイル名}_{id}.downloadfile……）.
			if ( !Directory.Exists("./Build/"+name) ){
				Directory.CreateDirectory("./Build/"+name);
			}
			string splitName = String.Format("./Build/"+name+"/"+name+"_{0}.downloadfile", num + 1);

			// byte配列のファイルへの書き込み.
			File.WriteAllBytes(splitName, dest);

			num++;
		}
		// 分割数を添える.
		File.WriteAllText("./Build/"+name+"/slice.txt",""+num);

		WWWForm form = new WWWForm();
		form.AddField("dir",App.name+"/zip/"+name+"/");
		form.AddField("time",System.DateTime.Now.ToBinary().ToString());
		using ( WWW www = new WWW(Server.SERVER_ROOT+"/create_directory.php",form) ){
			while ( www.isDone == false ){}
			if ( www.error != null ) Debug.Log(www.error);
			else Debug.Log("フォルダを作成した:"+App.name+"/zip/"+name+"/");
		}

		WWWForm form3 = new WWWForm();
		form3.AddField("dir",App.name+"/zip/"+name+"/");
		form3.AddField("filename","slice.txt");
		form3.AddField("time",System.DateTime.Now.ToBinary().ToString());
		form3.AddField("data",""+num);
		using ( WWW www = new WWW(Server.SERVER_ROOT+"/uploader.php",form3) ){
			while ( www.isDone == false ){}
			if ( www.error != null ) Debug.Log(www.error);
			else Debug.Log(www.text);
		}

		for (int i = 0; i < num; ++i ){
			WWWForm form2 = new WWWForm();
			string filename =  String.Format(""+name+"_{0}.downloadfile",i + 1);
			string splitName = String.Format("./Build/"+name+"/"+name+"_{0}.downloadfile", i + 1);
			form2.AddField("dir",App.name+"/zip/"+name+"/");
			form2.AddField("filename",filename);
			form2.AddField("data",System.Convert.ToBase64String(File.ReadAllBytes(splitName))); 
			form.AddField("time",System.DateTime.Now.ToBinary().ToString());
			using ( WWW www = new WWW(Server.SERVER_ROOT+"/uploader.php",form2) ){
				while ( www.isDone == false ){}
				if ( www.error != null ) Debug.Log(www.error);
				else Debug.Log(www.text );
			}
			// ProgressBar表示
			EditorUtility.DisplayProgressBar(
				"サーバに"+name+"のスライスしたzipファイルをアップロード中",
				""+i+"/"+num,
				(float)i/(float)num
			);
		}
		// 処理が終わってProgressBarを閉じる
		EditorUtility.ClearProgressBar();
		#endif
	}

	public static void Zip(string name){
		#if UNITY_EDITOR
		try{
			//ZIPクラスをインスタンス化.
			using (ZipFile zip = new ZipFile(Encoding.GetEncoding("utf-8"))){
				//圧縮レベルを設定.
				zip.CompressionLevel = CompressionLevel.BestCompression;

				if ( Directory.Exists("./Archive/"+name) ){
					//ディレクトリを追加する場合.
					zip.AddDirectory("./Archive/"+name, name);

					//ZIPファイルを保存.
					zip.Save("./Build/"+name+".zip");

					Debug.Log("ZIP Success "+name+".zip");

					SliceZip(name);
				}else Debug.Log("ディレクトリが存在しません."+name);
			}			

		}
		catch (Exception e){
			Debug.Log("ZIP Failed : "+e.Message);
		}
		#endif
	}

	public static void UnZip(string path, int splitcnt = 0){
		if ( Directory.Exists(path.Replace(".zip","").Replace("/zip","")) ){
			// 解凍済みのフォルダがある場合は解凍しない.
			//Debug.Log("Failed UnZip : Already UnZip : "+path);
			return;
		}
		if ( splitcnt == 0 ) ZipUtil.Unzip(path,Application.persistentDataPath);
		else{
			// 出力ファイル名（{ファイル名}_{id}.downloadfile……）.
			string name = Path.GetFileName(path).Replace(".zip","");
			string dir = Path.GetDirectoryName(path);

			// ダウンロードファイルをひとつにまとめる.
			byte[] downloadfile = new byte[1];
			for ( int i = 0; i < splitcnt; ++i ){
				string splitName = String.Format(dir+"/"+name+"_{0}.downloadfile", i + 1);
				byte[] src = System.Convert.FromBase64String(File.ReadAllText(splitName));
				int aryLen = downloadfile.Length;

				// 配列を拡張する.
				Array.Resize(ref downloadfile, downloadfile.Length + src.Length);
				if ( i == 0 ){
					downloadfile = src;
				}else{
					Array.Copy(src, 0, downloadfile, aryLen, src.Length);
				}
			}
			// byte配列のファイルへの書き込み.
			string path2 = dir+"/"+name+".zip";
			Debug.Log(path2);
			File.WriteAllBytes(path2, downloadfile);
			Debug.Log("Write Success");
			try{
				ZipUtil.Unzip(path2,Application.persistentDataPath);

				for ( int i = 0; i < splitcnt; ++i ){
					string splitName = String.Format(dir+"/"+name+"_{0}.downloadfile", i + 1);
					File.Delete(splitName);
				}
			}catch(Exception e){
				DialogManager.instance.SetLabel("OK","OK","OK");
				DialogManager.instance.ShowSubmitDialog(
					"Failed UnZIP : This zip is broken."+System.Environment.NewLine+
					e.ToString(),
					(res)=>{});
				return; 
			}
			Debug.Log("Unzip Success");
		}
	}
}