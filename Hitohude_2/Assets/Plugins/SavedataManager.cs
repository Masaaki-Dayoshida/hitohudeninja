﻿using UnityEngine;
using System.Collections;

public class SavedataManager: MonoBehaviour {

	static int _ClearStageNum = 0;
    static int _Rank = 1;
    static int _Point = 30;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void Load()
	{
        //PlayerPrefs.SetInt("Rank", 1);
        //PlayerPrefs.SetInt("ClearNum", 0);
        
        _ClearStageNum = PlayerPrefs.GetInt("ClearNum", 0);
        _Rank = PlayerPrefs.GetInt("Rank", 1);
        _Point = PlayerPrefs.GetInt("Point", 10);
	}

	public static void Save(int num)
	{
		_ClearStageNum = num;
        PlayerPrefs.SetInt("ClearNum", _ClearStageNum);
	}

	public static void SaveRank(int rank)
	{
		_Rank = rank;
        PlayerPrefs.SetInt("Rank", _Rank);
	}

	public static int ClearStageNum()
	{
		return _ClearStageNum;
	}

	public static int Rank()
	{
		return _Rank;
	}

    public static void ValuePoint(int value)
    {
        _Point += value;
		PlayerPrefs.SetInt("Point", _Point);
    }

    public static int GetPoint()
    {
        return _Point;
    }
}
