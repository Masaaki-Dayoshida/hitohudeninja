﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GoodPlaceFramework;

public class RankManager : MonoBehaviour {

	private Image image;

    private int Rank = 1;

    // Use this for initialization
	void Start () {
        if (Rank != 0)
        {
            image = GetComponent<Image>();
            image.sprite = ServerManager.DownloadSprite("Texture/Rank/rank" + Rank + ".png");
        }
        //ServerManager.instance.DownloadFile("http://frustration893.web.fc2.com/nikki/nikkigazou/1110/sabaki.jpg", "test.jpg");
    }

    // Update is called once per frame
	void Update () {
	}

	public void SetRank(int num)
	{
		Rank = num;
        if (Rank != 0)
        {
            image = GetComponent<Image>();
            image.sprite = ServerManager.DownloadSprite("Texture/Rank/rank" + Rank + ".png");
        }
    }
}
