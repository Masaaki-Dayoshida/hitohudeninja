﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IsBayText : MonoBehaviour {

    private Text uiText;

	// Use this for initialization
	void Start () {
        uiText = GetComponent<Text>();

        uiText.text = 
            "ポイントを使って答えを見ますか？" + 
            "10ポイント消費します" + 
            "現在10ポイント";
	}
	
	// Update is called once per frame
	void Update () {
        if (BayWindow.is_show == BayWindow.SHOW_STATE.STATE_NONE)
            return;

        string firstLine;
        string secondLine;
        string thirdLine;

        if (BayWindow.is_show == BayWindow.SHOW_STATE.STATE_HINT)
        {
            firstLine = "ポイントを使ってヒントを見ますか？\n";
            secondLine = "2ポイント消費します\n";
        }
        else
        {
            firstLine = "ポイントを使って答えを見ますか？\n";
            secondLine = "10ポイント消費します\n";
        }

        thirdLine = "現在" + SavedataManager.GetPoint().ToString() + "ポイント";

        uiText.text = firstLine + secondLine + thirdLine;
	
	}
}
