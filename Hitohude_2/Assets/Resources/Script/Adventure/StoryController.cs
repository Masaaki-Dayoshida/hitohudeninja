﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class StoryController : MonoBehaviour {

	public static StoryController instance;

	// コマンドの設定.
	string[] commandList = new string[]{
		"[wait]",		// 待つ.
		"[cm]",			// メッセージクリア.
		"[l]",			// タッチするまで待つ.
		"[p]",			// 文章の終わり.
		"[exit]", 		// 次のステージへ.
		"[bg]",			// 背景変更.
		"[image]",		// 画像表示.
		"[imageclear]"	// 画像消去.
	};
	
	// 文章エスケープ設定.
	string[] escapes = new string[]{
		"<br>","\n"		// 改行.
	};

	// ストーリープレハブ設定.
	string[] prefabList = new string[]{
		"BG",
		"Message",
		"MessageBox",
		"SkipText",
		"SkipButton",
	};	

	// 現在の表示スピード.
	public float displaySpeed = 0.1f;

	 [System.NonSerialized]
    public string nextScene = "StageSelect1";

	// スキップ.
	public bool isSkip = false;

	// 読み込んだテキスト.
	public string[] parses;

	char newline = "\n"[0];
	Dictionary<string,GameObject> o;
	TextAsset ta;
	TextMesh tm;
	Dictionary<string,bool> command = new Dictionary<string,bool>();
	string stockMessage = "";
	int stockMessageLen = 0; 
	MessageBox mb;

// ===== コマンド登録.
	
	IEnumerator Interpretation(string parse){
		if ( command["[wait]"] )
		{
			yield return StartCoroutine(CommandWait(parse));
		}
		else if ( command["[cm]"] )
		{
			tm.text = "";
		}
		else if ( command["[exit]"] )
		{
			SceneChanger.ChangeScene(nextScene);
		}
		else if ( command["[bg]"] )
		{
			yield return StartCoroutine(CommandBackGroundChange(parse));
		}
		else if ( command["[image]"] )
		{
			yield return StartCoroutine(CommandImage(parse));
		}
		else if ( command["[imageclear]"] )
		{
			// 画像の一括削除.
			GameObject[] objects = GameObject.FindGameObjectsWithTag("StoryImage");
			foreach(GameObject go in objects) Destroy(go);
		}
		else
		{
			stockMessage = Convert(parse);
			stockMessageLen = stockMessage.Length;
			yield break;
		}

		isSkip = false;
		yield break;
	}

	IEnumerator PostInterpretation(string parse){
 		if ( command["[l]"])
		{
			yield return StartCoroutine(CommandClickWait());
			yield return null;
		}
	}

// ===== 各コマンドの特殊処理.
	
	IEnumerator CommandWait(string src){
		string[] param = src.Split("|"[0]);
		if ( param.Length < 2 ) yield break;
		float time = 0f;
		float.TryParse(param[1],out time);
		yield return new WaitForSeconds(time);
	}

	IEnumerator CommandClickWait(){
		while(!mb.isClick){
			yield return null;
		}
	}

	IEnumerator CommandBackGroundChange(string src){
		string[] param = src.Split("|"[0]);
		if ( param.Length < 2 ) yield break;

        param[1] = param[1].Replace("\r", "");
        
        SpriteRenderer sr = o["BG"].GetComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("Texture/story/" + param[1]);
    }

	Vector3 StringToVector3(string str){
		if ( str == "" ){
			return Vector3.zero;
		}
		string[] tmp = str.Split(","[0]);
		float x,y,z;
		x = 0; y = 0; z = 0;
		if ( tmp.Length >= 1 ) float.TryParse(tmp[0], out x);
		if ( tmp.Length >= 2 ) float.TryParse(tmp[1], out y);
		if ( tmp.Length >= 3 ) float.TryParse(tmp[2], out z);
		return new Vector3(x,y,z);
	}

	Quaternion StringToQuaternion(string str){
		if ( str == "" ){
			return Quaternion.identity;
		}
		string[] tmp = str.Split(","[0]);
		float x,y,z,w;
		x = 0; y = 0; z = 0; w = 0;
		if ( tmp.Length >= 1 ) float.TryParse(tmp[0], out x);
		if ( tmp.Length >= 2 ) float.TryParse(tmp[1], out y);
		if ( tmp.Length >= 3 ) float.TryParse(tmp[2], out z);
		if ( tmp.Length >= 4 ) float.TryParse(tmp[3], out w);
		return new Quaternion(x,y,z,w);
	}

	Color StringToColor(string str){
		if ( str == "" ){
			return Color.white;
		}
		string[] tmp = str.Split(","[0]);
		float r,g,b,a;
		r = 0; g = 0; b = 0; a = 0;
		if ( tmp.Length >= 1 ) float.TryParse(tmp[0], out r);
		if ( tmp.Length >= 2 ) float.TryParse(tmp[1], out g);
		if ( tmp.Length >= 3 ) float.TryParse(tmp[2], out b);
		if ( tmp.Length >= 4 ) float.TryParse(tmp[3], out a);
		return new Color(r,g,b,a);
	}

	IEnumerator CommandImage(string src){
		string[] param = src.Split("|"[0]);
		string filepath = "";
		string name = "image";
		Vector3 pos = Vector3.zero;
		Quaternion rot = Quaternion.identity;
		Vector3 scale = new Vector3(1f,1f,1f);
		Color col = Color.white;
		foreach ( string val in param ){
			if ( val == "" ) continue;
			if ( val.IndexOf("storage=") >= 0 ) filepath = val.Replace("storage=","");
			else if ( val.IndexOf("name=") >= 0 ) name = val.Replace("name=","");
			else if ( val.IndexOf("pos=") >= 0 ) pos = StringToVector3(val.Replace("pos=",""));
			else if ( val.IndexOf("rot=") >= 0 ) rot = StringToQuaternion(val.Replace("rot=",""));
			else if ( val.IndexOf("scale=") >= 0 ) scale = StringToVector3(val.Replace("scale=",""));
			else if ( val.IndexOf("color=") >= 0 ) col = StringToColor(val.Replace("color=",""));
			else if ( val.IndexOf("[image]") >= 0 ) ;
			else
			{
				Debug.Log("文法エラー [image]:"+val);
				yield break;
			}
		}

		GameObject targetObj = null;

		// 同じ名前のオブジェクトがあったら、それを更新.
		GameObject[] objects = GameObject.FindGameObjectsWithTag("StoryImage");
		foreach(GameObject go in objects){
			if( go.name == name ){
				targetObj = go;
				break;
			}
		}
		SpriteRenderer srChara;
		if ( targetObj == null ){
			// 新規作成.
			targetObj = new GameObject();
			srChara = targetObj.AddComponent<SpriteRenderer>();
			targetObj.transform.position = pos;
			targetObj.transform.rotation = rot;
			targetObj.transform.localScale = scale;
            filepath = filepath.Replace("\r", "");
            srChara.sprite = Resources.Load<Sprite>("Texture/story/" + filepath);
            
			srChara.color = col;
			targetObj.name = name;
			targetObj.tag = "StoryImage";
			targetObj.transform.parent = transform;

		}else{
			// 更新.
			srChara = targetObj.GetComponent<SpriteRenderer>();
			targetObj.transform.position = pos;
			targetObj.transform.rotation = rot;
			targetObj.transform.localScale = scale;
            filepath = filepath.Replace("\r", "");
            srChara.sprite = Resources.Load<Sprite>("Texture/story/" + param[1]);
            srChara.color = col;
		}
	}


// ====================================================

	void Start () {
		// ストーリーテキスト読み込み.
        int num = SavedataManager.ClearStageNum();
        int stageNum = (num / 10);

        if ( stageNum <= 0 ) stageNum = 1;
		ta = Resources.Load<TextAsset>("CSV/storytext/stage"+stageNum);
		if ( ta == null ){
			Debug.Log("StoryController : not found text : stage"+stageNum);
			SceneChanger.ChangeScene(nextScene);
			return;
		}
		
		// シングルトン化.
		Singleton();

		// プレハブの配置.
		InitPrefabControl();
			
		// コマンド初期化.
		foreach ( string commandName in commandList ){
			if ( command.ContainsKey(commandName) ){
				command[commandName] = false;
			}else{
				command.Add(commandName, false);
			}			
		}

		// テキストの初期化.
		tm = o["Message"].GetComponent<TextMesh>();

		// あたり判定の初期化.
		mb = o["MessageBox"].GetComponent<MessageBox>();
		
		// コンパイル.
		StartCoroutine(Compile());
	}
	
	IEnumerator Compile(){
		string src = ta.text;
		
		// 改行ごとに構文解析>解釈>実行>待機.
		parses = src.Split(newline);
		foreach ( string parse in parses ){
			
			// 構文解析.
			foreach ( string commandName in commandList ){
				if ( parse.IndexOf(commandName) != -1 ) command[commandName] = true;
				else command[commandName] = false;
			}

			// メッセージバッファの初期化.
			stockMessage = "";
			stockMessageLen = 0;

			// 構文解釈実行.
			yield return StartCoroutine(Interpretation(parse));
			
			// 表示待機.
			for ( int i = 0; i < stockMessageLen; ++i ){

				tm.text += stockMessage.Substring(i,1);
				// スキップ処理.
                if (i == stockMessageLen - 1)
                {
                    if (mb.isClick)
                    {
                        isSkip = true;
                        mb.isClick = false;

                    }
                }
                else 
                {
                    if (i == 0)
                    {
                        mb.isClick = false;
                    
                    }

                    if (mb.isClick)
                    {
                        mb.isClick = false;
                        // iを初期化でインクリメントしないと直前の文字が出る
                        for (i ++; i < stockMessageLen; ++i)
                        {
                            tm.text += stockMessage.Substring(i, 1);
                        }
                    }
                }

                yield return new WaitForSeconds(displaySpeed);

			}

			// 表示後実行.
			yield return StartCoroutine(PostInterpretation(parse));		
		}
	}

	// シングルトン化.
	void Singleton(){
		if ( instance == this ){
			Destroy(gameObject);
			return;
		}
		instance = this;
	}

	// ビューの配置.
	void InitPrefabControl(){
		o = new Dictionary<string,GameObject>();
		foreach( string prefabName in prefabList ){
			o.Add(prefabName,Instantiate(Resources.Load("Prefabs/story/"+prefabName)) as GameObject);
			o[prefabName].transform.parent = transform;
			o[prefabName].name = prefabName;
		}
	}

	// 表示用に変換.
	string Convert(string src){
		// コマンドは文章に含まない.
		foreach ( string commandName in commandList ){
			src = src.Replace(commandName,"");
		}
		if ( escapes.Length % 2 == 1 ){
			Debug.LogError("escapesの設定がおかしい");
			return src;
		}
		
		// 解析中に消えてしまう文字の付与.
		for ( int i = 0; i < escapes.Length; i += 2 ){
			src = src.Replace(escapes[i],escapes[i+1]);
		}
		return src;
	}

}
