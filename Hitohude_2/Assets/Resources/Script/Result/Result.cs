﻿using UnityEngine;
using System.Collections;

public class Result : MonoBehaviour {

	static int currentStage = 0;


	// Use this for initialization
	void Start () {
		int num = SavedataManager.ClearStageNum();
		if (currentStage > num)
		{
			SavedataManager.Save(currentStage);
			num = currentStage;
		}
		num = SavedataManager.Rank();
		RankManager obj = FindObjectOfType<RankManager>();
		obj.SetRank(num);
		AdBanner._Instance.Show();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			AdBanner._Instance.Hide();
			SceneChanger.ChangeScene("StageSelect1");
		}
	
	}

	public void NextStage()
	{
		audio.Play();
		AdBanner._Instance.Hide();
		int num = SavedataManager.ClearStageNum()/10+1;
		if (SavedataManager.Rank() < num)
		{
			RankUpEvent.SetNext("Game", currentStage + 1);
			SceneChanger.ChangeScene("RankupEvent");
		}
		else
		{
			SceneChanger.ChangeScene("Game");
			ListManager.SetStageNum(currentStage + 1);
		}
	}

	static public void SetCurrentStageNum(int num)
	{
		currentStage = num;
		SavedataManager.Save(currentStage+1);
	}
}
