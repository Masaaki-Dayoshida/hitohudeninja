﻿using UnityEngine;
using System.Collections;

public class PageManager : MonoBehaviour {

	public float pageOffsetX = -12.0f;
	public int pageMax;
	Vector3 localPos;
	float startX = 0;
	float destPosX = 0;
	static int selectPage = 0;
	bool Moving = false;
	private int CurrentRank;

	// Use this for initialization
	void Start () {
		localPos = transform.position;
		destPosX = localPos.x;
		selectPage = 0;
		Moving = false;
		startX = 0;
		CurrentRank = SavedataManager.Rank();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			startX = Input.mousePosition.x;
		}

		if (Input.GetMouseButton(0))
		{
			if (Moving == false)
			{
				float sub = (Input.mousePosition.x - startX);
				if (sub > 50.0f)
				{
					Moving = true;
					selectPage--;

				}
				else if (sub < -50.0f)
				{
					if (selectPage < CurrentRank-1)
					{
						Moving = true;
						selectPage++;
					}
				}
				if (selectPage < 0)
				{
					selectPage = 0;
				}
				else if (selectPage > pageMax - 1)
				{
					selectPage = pageMax - 1;
				}
				destPosX = selectPage * pageOffsetX;
			}
			else
			{
				float sub = Mathf.Abs(destPosX - transform.position.x);
				if (sub < 2.0f)
				{
					Moving = false;
				}
			}
		}
		Vector3 pos = transform.position;
		pos.x += (destPosX - pos.x) * 0.1f;
		transform.position = pos;
	
	}

	public void AddNum(int num)
	{
		selectPage += num;
		if (selectPage > CurrentRank-1)
		{
			selectPage -= num;
		}

		if (selectPage < 0)
		{
			selectPage = 0;
		}
		else if (selectPage >= pageMax)
		{
			selectPage = pageMax - 1;
		}

		destPosX = pageOffsetX * selectPage;
	}

	static public int SelectPageNum()
	{
		return selectPage;
	}
}
