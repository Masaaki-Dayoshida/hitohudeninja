﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Title : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SavedataManager.Load();
		int num = SavedataManager.Rank();
		RankManager obj = FindObjectOfType<RankManager>();
		obj.SetRank(num);
		AdBanner._Instance.Show();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	public void toStageSelect()
	{
		audio.Play();
		SceneChanger.ChangeScene("StageSelect1");
		AdBanner._Instance.Hide();
	}
}
