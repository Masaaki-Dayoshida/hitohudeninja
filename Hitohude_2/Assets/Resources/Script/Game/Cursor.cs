﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Cursor : MonoBehaviour {

	public GameObject Line;
	Vector3 pos;
	Vector3 lineHead;
	Camera cam;
    int ActivePointerID = -1;
    List<int> ConnectList = new List<int>();
	List<bool> flagList = new List<bool>();
    bool _Active = false;
	bool _First = true;
	bool playable = true;
	LinePoint StartPoint = null;
	LineRenderer selfLine;

    List<LinePoint> histroy = new List<LinePoint>();
    List<SolidLine> lineHistroy = new List<SolidLine>();

	// Use this for initialization
	void Start () 
    {
        ConnectList.Clear();
        flagList.Clear();
        histroy.Clear();
        lineHistroy.Clear();

		cam = GameObject.FindObjectOfType<Camera> ();
		selfLine = GetComponent<LineRenderer> ();
		lineHead = transform.position;
		lineHead.z = -1.0f;
		selfLine.SetPosition (0, lineHead);
		selfLine.SetPosition (1, lineHead);
	}
	
	// Update is called once per frame
	void Update () 
    {
		float y = Mathf.Repeat (Time.time, 1);
		
		// Yの値がずれていくオフセットを作成
		Vector2 offset = new Vector2 (y, y*1.5f);
		selfLine.material.SetTextureOffset ("_MainTex", offset);

		if (playable)
		{
			if (Input.GetMouseButton(0))
			{
				pos = Input.mousePosition;
				Vector3 position = cam.ScreenToWorldPoint(pos);
				position.z = 0;
				gameObject.transform.position = position;
				renderer.enabled = true;
				if (_Active)
				{
					position.z = -1;
					selfLine.SetPosition(1, position);
				}
			}
			else if (Input.GetMouseButtonUp(0))
			{
				_Active = false;
				if (_First)
				{
					ActivePointerID = -1;
					ConnectList = null;
					flagList = null;

					if (StartPoint != null)
					{
						StartPoint.SetActive(false);
					}

					histroy.Clear();
				}
				selfLine.SetPosition(1, lineHead);
			}
			else
			{
				renderer.enabled = false;
			}
		}
		else
		{
			Vector3 position = transform.position;
			position.z = -1;
			selfLine.SetPosition(1, position);
		}
	}

    public bool Active()
    {
        return _Active;
    }

    public void SetActive(bool flag)
    {
        _Active = flag;
        if (flag == false)
        {
            ActivePointerID = -1;
            ConnectList = null;
            flagList = null;
			if (StartPoint != null)
			{
				StartPoint.SetActive(false);
			}
            _Active = false;
            selfLine.SetPosition(1, lineHead);

            lineHistroy.Clear();

            histroy.Clear();

        }
    }

    public void UnDo()
    {
        if (ActivePointerID>=0)
        {
            StartPoint.SetActive(false);
            int last = histroy.Count - 1;
            
            if (last < 0)
            {
                return;
            }

            histroy.RemoveAt(last);
            StartPoint = histroy[last - 1];

            StartPoint.SetActive(true);
            ActivePointerID = StartPoint.GetId();
            lineHead = StartPoint.transform.position;
            selfLine.SetPosition(0, lineHead);

            last--;

            lineHistroy[last].Release();
            lineHistroy.RemoveAt(last);

            if (last == 0)
            {
                ActivePointerID = -1;
                ConnectList = null;
                flagList = null;
                StartPoint.SetActive(false);
                _Active = false;
                histroy.Clear();
                lineHistroy.Clear();
                _First = true;
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(ActivePointerID);
        if(other.tag == "LinePoint")
        {
			if(!_Active)
			{
                if (ActivePointerID < 0)
                {
                    StartPoint = other.GetComponent<LinePoint>();
                    if (StartPoint != null)
                    {
                        ActivePointerID = StartPoint.GetId();
                        ConnectList = StartPoint.GetList();
                        flagList = StartPoint.GetFlagList();
                        _Active = true;
                        StartPoint.SetActive(true);
                        lineHead = StartPoint.transform.position;
                        lineHead.z = -1;
                        selfLine.SetPosition(0, lineHead);
                        histroy.Add(StartPoint);

                    }
                }
                else
                {
                    if (other.GetComponent<LinePoint>().GetId() == ActivePointerID)
                    {
                        _Active = true;
                    }
                }
			}
			else
			{
				_First = false;
				LinePoint point = other.GetComponent<LinePoint>();
				if(point == null)
				{
					return ;
				}

                List<int> otherList = new List<int>();
				List<bool> otherFlagList = new List<bool>();

				otherList = point.GetList();
				otherFlagList = point.GetFlagList();
				int parentIndex = ConnectList.IndexOf(point.GetId());
				int otherIndex = otherList.IndexOf(ActivePointerID);

                if (ListManager.AnswerMode == true)
                {
                    int current_answer = GameObject.Find("ListManager").GetComponent<ListManager>().current_answer();
                    if (current_answer != point.GetId())
                        return;
                }
                
                short taskID = 0;
				if(parentIndex >= 0)
				{
					taskID += 1;
				}
				if(otherIndex >= 0)
				{
					taskID += 2;
				}
				bool noConnect = false;
				switch(taskID)
				{
				case 1:
					if(flagList[parentIndex] == false)
					{
						noConnect = true;
						flagList[parentIndex] = true;
					}
					break;
				case 2:
					if(otherFlagList[otherIndex] == false)
					{
						noConnect = true;
						otherFlagList[otherIndex] = true;
					}
					break;
				case 3:
					if(flagList[parentIndex] == false || otherFlagList[otherIndex] == false)
					{
						noConnect = true;
						flagList[parentIndex] = true;
						otherFlagList[otherIndex] = true;
					}
					break;
				default:
					break;
				}
				if(noConnect)
				{
					Vector3 headPos = StartPoint.transform.position;
					headPos.z = 1;
					GameObject obj = Instantiate(Line,headPos,Quaternion.identity) as GameObject;
					if(obj != null)
					{
						SolidLine line = obj.GetComponent<SolidLine>();
						if(line != null)
						{
							Vector3 tailPos = point.transform.position;
							tailPos.z = 1;
                            line.SetPoint(StartPoint, point);
                            lineHistroy.Add(line);
                            histroy.Add(point);
						}
					}
					else{
						return;
					}
					audio.Play();
					//StartPoint.SetTailPos(point.transform.position);
					StartPoint.SetActive (false);

					ActivePointerID = point.GetId();
					ConnectList = otherList;
					flagList = otherFlagList;
					point.SetActive(true);

					StartPoint = point;
					lineHead = StartPoint.transform.position;
					lineHead.z = -1;
					selfLine.SetPosition(0,lineHead);

				}

			}//if(_Active == true)
        }//if(tag == "LinePoint")
    }

    public void SetActivePointer(int num,List<int> list,Vector3 point)
    {
        if (!_Active)
        {
            ActivePointerID = num;
            ConnectList = list;
			_Active = true;
        }
    }


	public void setPlayable(bool flag)
	{
		playable = flag;
	}

}
