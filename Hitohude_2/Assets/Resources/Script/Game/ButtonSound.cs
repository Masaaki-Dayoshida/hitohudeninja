﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour {

	AudioSource[] se;
	// Use this for initialization
	void Start () {
		se = GetComponentsInChildren<AudioSource>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Play(int num)
	{
		if (se[num] != null)
		{
			se[num].Play();
		}
	}
}
