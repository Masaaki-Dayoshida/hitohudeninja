﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ListManager : MonoBehaviour
{

    public GameObject linePoint;
    public GameObject Line;
    public GameObject clearText;
	public GameObject hintEffect;
	public GameObject Number;
	public GameObject HintObject;
    List<LinePoint> pointList = new List<LinePoint>();
	List<int> answer = new List<int>();
    int pointNum = 5;
    int connectNum = 2;
	static int stageNum = 0;
	static public bool AnswerMode = false;
	int AnswerNum = 0;

    public int current_answer()
    { 
        return answer[AnswerNum];
    }

    // Use this for initialization
    void Start()
    {
        AnswerMode = false;
        //GeneratePoint ();
		answer.Clear();
        CreateFromCSV();
		ReadAnswerFile();
    }
    // Update is called once per frame
    void Update()
    {
        LinePoint[] point = GameObject.FindObjectsOfType<LinePoint>();
        int num = point.Length;
        bool clear = true;

        switch(BayWindow.is_bay)
        {
            case BayWindow.BAY_STATE.STATE_NONE:
                break;
            case BayWindow.BAY_STATE.STATE_ANSWER_BAY:
        
                if (!AnswerMode)
		        {
                    if (HintObject != null)
                    SavedataManager.ValuePoint(-10);
                    FindObjectOfType<LineManager>().AllSolidRelease();
			        Vector3 pos = pointList[answer[AnswerNum]].transform.position;
			        if (HintObject == null)
			        {
				        HintObject = Instantiate(hintEffect, pos, Quaternion.identity) as GameObject;
			        }
			        else
			        {
				        HintObject.transform.position = pos;
			        }
			        AnswerMode = true;
		        }
                BayWindow.ResetBayState();
                break;
            case BayWindow.BAY_STATE.STATE_HINT_BAY:
        
                if (HintObject == null)
		        {
                    SavedataManager.ValuePoint(-2);
                    Vector3 pos = pointList[answer[0]].transform.position;
			        HintObject = Instantiate(hintEffect, pos, Quaternion.identity) as GameObject;
		        }
                BayWindow.ResetBayState();
                break;
            case BayWindow.BAY_STATE.STATE_NO_BAY:
                BayWindow.ResetBayState();
                break;
        }

		if (AnswerMode)
		{
			if (AnswerNum < answer.Count-1)
			{
				if (pointList[answer[AnswerNum]].Active())
				{
					AnswerNum++;
				}
				Debug.Log(answer.Count);
				Debug.Log(AnswerNum);
				HintObject.transform.position = pointList[answer[AnswerNum]].transform.position;
			}
			
		}
        for (int cnt = 0; cnt < num; cnt++)
        {
            if (point[cnt].Complete() == false)
            {
                clear = false;
                break;
            }
        }
        if (clear)
        {
            Text text = clearText.GetComponent<Text>();
            text.text = "Clear!";
			SceneChanger sc = GetComponent<SceneChanger>();
			sc.FadeOutWaitTime = 0.8f;
			sc.FadeOutDelayTime = 2.0f;
			Result.SetCurrentStageNum(stageNum);
			AdBanner._Instance.Hide();
			SceneChanger.ChangeScene("Result");
			GameObject obj = GameObject.FindWithTag("Cursor");
			if (obj != null)
			{
				Destroy(obj);
			}
			obj = GameObject.FindWithTag("DeleteTarget");
			if (obj != null)
			{
				Destroy(obj);
			}

        }
        else
        {
            Text text = clearText.GetComponent<Text>();
            text.text = "Thinking";
        }

    }

    void GeneratePoint()
    {
        List<LinePoint> pointList = new List<LinePoint>();
        for (int cnt = 0; cnt < pointNum; cnt++)
        {
            Vector3 pos;
            pos.x = Random.Range(-5.0f, 5.0f);
            pos.y = Random.Range(-5.0f, 5.0f);
            pos.z = 0;
            GameObject obj = GameObject.Instantiate(linePoint, pos, Quaternion.identity) as GameObject;
            LinePoint point = obj.GetComponent<LinePoint>();
            for (int num = 0; num < connectNum; num++)
            {
                point.AddConnect(Random.Range(0, pointNum - 1));
            }
            pointList.Add(point);
        }


    }

    void CreateShadow()
    {
        int Count = 0;
        for (int cnt = 0; cnt < pointNum; cnt++)
        {
            List<int> connectList = pointList[cnt].GetList();
            List<bool> flagList = pointList[cnt].GetFlagList();
            int connectCount = connectList.Count;
            for (int num = 0; num < connectCount; num++)
            {
                int parentIndex = connectList[num];
                int otherIndex = pointList[parentIndex].GetList().IndexOf(cnt);
                
                List<bool> otherFlagList = pointList[parentIndex].GetFlagList();
                if (otherIndex >= 0)
                {
                    if (otherFlagList[otherIndex])
                    {
                        otherFlagList[otherIndex] = false;
                    }
                    else
                    {
                        flagList[num] = false;
                        continue;
                    }
                }
                if (flagList[num] == false)
                {
                    continue;
                }
                Vector3 TailPos = pointList[parentIndex].transform.position;
                Vector3 HeadPos = pointList[cnt].transform.position;
                TailPos.z = 2;
                HeadPos.z = 2;
                pointList[cnt].GetFlagList()[num] = false;
                GameObject obj = GameObject.Instantiate(Line, HeadPos, Quaternion.identity) as GameObject;
                LineShadow line = obj.GetComponent<LineShadow>();
                Count++;
                if (line != null)
                {
                    line.SetTailPos(TailPos);
                    line.SetColors(Color.gray, Color.gray);
                }

            }
        }
        Debug.Log(Count);
    }
    private void CreateFromCSV()
    {
        TextAsset csv = Resources.Load("CSV/stage"+ (stageNum+1)) as TextAsset;
		if (csv == null)
		{
			csv = Resources.Load("CSV/test") as TextAsset;
		}
        string[] lineArray = csv.text.Replace("\r\n", "\n").Split('\n');
        List<string> dataList = new List<string>(lineArray);
        int lineNum = 0;
        int count = 0;

        string[] file_data = dataList[lineNum].Split(","[0]);

        pointNum = dataList.Count-1;

        while (lineNum < pointNum)
        {
            count = 0;
            file_data = dataList[lineNum].Split(","[0]);

            float PosX = 0;
            float PosY = 0;
            if (float.TryParse(file_data[count].ToString(), out PosX) == false || float.TryParse(file_data[count + 1].ToString(), out PosY) == false)
            {
                lineNum++;
                continue;
            }
            Vector3 pos = new Vector3(PosX, PosY, -1);
            count += 2;
            int buff = 0;
            int columnNum = file_data.GetLength(0);

            GameObject obj = GameObject.Instantiate(linePoint, pos, Quaternion.identity) as GameObject;
            LinePoint point = obj.GetComponent<LinePoint>();
           // point.Initialize();
            for (int num = 0; num < connectNum; num++)
            {
                point.AddConnect(Random.Range(0, pointNum - 1));
            }
            pointList.Add(point);
            point.GetList().Clear();
           // point.GetFlagList().Clear();

            while (count < columnNum)
            {
                if (int.TryParse(file_data[count].ToString(), out buff) == true)
                {
                    point.AddConnect(buff);
                    count++;
                    
                }
                else
                {
                    break;
                }//else_end
            }//while_end

            lineNum++;

        }//while_end
        CreateShadow();
    }

	void ReadAnswerFile()
	{
		TextAsset csv = Resources.Load("CSV/Answer/answer" + (stageNum + 1)) as TextAsset;
		if (csv == null)
		{
			csv = Resources.Load("CSV/Answer/answer_test") as TextAsset;
		}
		string[] lineArray = csv.text.Replace("\r\n", "\n").Split('\n');
		List<string> dataList = new List<string>(lineArray);
		int lineNum = 0;
		int num = 0;
		string[] file_data = dataList[lineNum].Split(","[0]);
		int charCount = file_data.GetLength(0);
		Debug.Log(charCount);

		for(int cnt=0;cnt<charCount;cnt++)
		{
			if (int.TryParse(file_data[cnt].ToString(),out num)==false)
			{
				break;
			}
			answer.Add(num);
		}

	}
	static public void SetStageNum(int num)
	{
        if (stageNum < num)
    		stageNum = num;
	}

   
	public void SetHint()
	{
        if (SavedataManager.GetPoint() < 2)
        {
            return;
        }

        BayWindow.is_show = BayWindow.SHOW_STATE.STATE_HINT;
	}

	public void Answer()
	{
        if (SavedataManager.GetPoint() < 10)
        {
            return;
        }

        BayWindow.is_show = BayWindow.SHOW_STATE.STATE_ANSWER;
	}

	public void ResetAnswer()
	{
		if (AnswerMode)
		{
			AnswerNum = 0;
			HintObject.transform.position = pointList[0].transform.position;
		}
	}
}
