﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AlbamImage : MonoBehaviour {

    private Image image;
    public int rank = 1;
    
    // Use this for initialization
	void Start () {
        if (rank <= SavedataManager.Rank())
        {
            image = GetComponent<Image>();
            image.color = Color.white;
            image.sprite = ServerManager.DownloadSprite("Texture/Rank/rank" + rank + ".png");
        }

	}
	
	// Update is called once per frame
	void Update () {
	}
}
