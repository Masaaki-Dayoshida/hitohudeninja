using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
//using GoodPlaceFramework;

public class DialogManager : MonoBehaviour
{
	public static DialogManager instance = null;
	Dictionary<int, System.Action<bool>> _delegates;	
	
	void Awake ()
	{
        if (instance!=null){
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
		_delegates = new Dictionary<int, Action<bool>> ();
		// set default label
		SetLabel("YES", "NO", "CLOSE");
	}
	
	void OnDestroy ()
	{
		if (_delegates != null) {
			_delegates.Clear ();
			_delegates = null;
		}
	}

	#if UNITY_IPHONE
	[DllImport("__Internal")]
    private static extern int _showSelectDialog (string msg);
	[DllImport("__Internal")]
	private static extern int _showSelectTitleDialog (string title, string msg);
	[DllImport("__Internal")]
	private static extern int _showSubmitDialog (string msg);
	[DllImport("__Internal")]
	private static extern int _showSubmitTitleDialog (string title, string msg);
	[DllImport("__Internal")]
	private static extern void _dissmissDialog (int id);
	[DllImport("__Internal")]
	private static extern void _setLabel(string decide, string cancel, string close);
	#endif

	public void AddChip(int chip, string mes){
		//User.chip += chip;
		//ShowSubmitDialog(mes,(bool res)=>{
		//	StartCoroutine(ServerManager.instance.LocalToServer());
		//});
	}
	
	public int ShowSelectDialog (string msg, Action<bool> del)
	{
		msg = msg.Replace("\\",System.Environment.NewLine);
		int id;
		#if UNITY_EDITOR
		id = 0;
		bool result = EditorUtility.DisplayDialog("",msg,m_cancel,m_decide);
		del(!result);
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            id = cls.CallStatic<int>("ShowSelectDialog", msg);
			_delegates.Add(id, del);
        }	
		#elif UNITY_IPHONE
			id = _showSelectDialog(msg);
			_delegates.Add(id, del);
		#endif
		return id;
	}
	
	public int ShowSelectDialog (string title, string msg, Action<bool> del)
	{
		msg = msg.Replace("\\",System.Environment.NewLine);
		int id;
		#if UNITY_EDITOR
		id = 0;
		bool result = EditorUtility.DisplayDialog(title,msg,m_cancel,m_decide);
		del(!result);
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            id = cls.CallStatic<int>("ShowSelectTitleDialog", title, msg);
			_delegates.Add(id, del);
        }	
		#elif UNITY_IPHONE
			id = _showSelectTitleDialog(title, msg);
			_delegates.Add(id, del);
		#endif
		return id;
	}
	
	public int ShowSubmitDialog (string msg, Action<bool> del)
	{
		msg = msg.Replace("\\",System.Environment.NewLine);
		int id;
		#if UNITY_EDITOR
		id = 0;
		bool result = EditorUtility.DisplayDialog("",msg,m_close);
		del(!result);
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            id = cls.CallStatic<int>("ShowSubmitDialog", msg);
			_delegates.Add(id, del);
        }
		#elif UNITY_IPHONE
			id = _showSubmitDialog(msg);
			_delegates.Add(id, del);
		#endif
		return id;
	}
	
	public int ShowSubmitDialog (string title, string msg, Action<bool> del)
	{
		msg = msg.Replace("\\",System.Environment.NewLine);
		int id;
		#if UNITY_EDITOR
			id = 0;
		bool result =EditorUtility.DisplayDialog(title,msg,m_close);
		del(!result);
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            id = cls.CallStatic<int>("ShowSubmitTitleDialog", title, msg);
			_delegates.Add(id, del);
        }
		#elif UNITY_IPHONE
			id = _showSubmitTitleDialog(title, msg);
			_delegates.Add(id, del);
		#endif
		return id;
	}
	
	public void DissmissDialog(int id) {
		#if UNITY_EDITOR
		
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            cls.CallStatic("DissmissDialog", id);
        }
		#elif UNITY_IPHONE
			_dissmissDialog(id);
		#endif
		
		if(_delegates.ContainsKey(id)) {
			_delegates [id] (false);
			_delegates.Remove (id);
		} else {
			Debug.LogWarning ("undefined id:" + id);
		}
	}
	#if UNITY_EDITOR
		string m_decide;
		string m_cancel;
		string m_close;
	#endif
	public void SetLabel(string decide, string cancel, string close) {
		#if UNITY_EDITOR
			m_decide = decide;
			m_cancel = cancel;
			m_close = close;
		#elif UNITY_ANDROID
		using (AndroidJavaClass cls = new AndroidJavaClass("unity.plugins.dialog.DialogManager")) {
            cls.CallStatic("SetLabel", decide, cancel, close);
        }
		#elif UNITY_IPHONE
			_setLabel(decide, cancel, close);
		#endif
		
	}
	
	#region Invoked from Native Plugin
	public void OnSubmit (string idStr)
	{
		int id = int.Parse (idStr);
		if (_delegates.ContainsKey (id)) {
			_delegates [id] (true);
			_delegates.Remove (id);
		} else {
			Debug.LogWarning ("undefined id:" + idStr);
		}
	}

	public void OnCancel (string idStr)
	{
		int id = int.Parse (idStr);
		if (_delegates.ContainsKey (id)) {
			_delegates [id] (false);
			_delegates.Remove (id);
		} else {
			Debug.LogWarning ("undefined id:" + idStr);
		}
	}

	public bool OnExists(string idStr)
	{
		int id = int.Parse (idStr);
		if (_delegates.ContainsKey (id)) return true;
		else return false;
	}
	#endregion
}

