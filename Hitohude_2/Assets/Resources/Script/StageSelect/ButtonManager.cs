﻿using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour {

	static int headNum = 0;
	public GameObject PageTitle;
	// Use this for initialization
	void Start () {
		SetNumber();
		SetButtonID();
		SpriteRenderer sprite = PageTitle.GetComponent<SpriteRenderer>();
		int num = headNum/10 +1;
        sprite.sprite = ServerManager.DownloadSprite("Texture/StageSelect/PageTitle/page_title" + num + ".png");


	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	static public void SetPade(int num)
	{
		headNum = num*10;
	}

	void SetNumber()
	{
		number[] Number = GetComponentsInChildren<number>();
		int num = Number.GetLength(0);

		for(int cnt=0; cnt < num ;cnt++)
		{
			Number[cnt].SetNum(1 + cnt);
		}
	}

	void SetButtonID()
	{
		button[] Button = GetComponentsInChildren<button>();
		int num = Button.GetLength(0);

		for (int cnt = 0; cnt < num; cnt++)
		{
			Button[cnt].SetNum(headNum + cnt);
		}
	}
}
