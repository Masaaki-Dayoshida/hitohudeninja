﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AdBanner._Instance.Show();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Return();
		}
	}

	void Return()
	{
		AdBanner._Instance.Hide();
		SceneChanger.ChangeScene("StageSelect1");
	}
}
