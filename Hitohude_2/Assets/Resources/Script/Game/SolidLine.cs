﻿using UnityEngine;
using System.Collections;

public class SolidLine : MonoBehaviour
{

    LineRenderer line = null;
    Vector3 tailPos;
    Vector3 destTailPos;
    bool ReleaseFlag;
    LinePoint start, end;
    int startID;//始点から接続されている終点のリストID
    int endID;//終点から接続されている終点のリストID

    // Use this for initialization
    void Start()
    {
        line = GetComponent<LineRenderer>();
        Vector3 pos = transform.position;
        tailPos = pos;
        line.SetPosition(0, pos);
        line.SetPosition(1, pos);
    }

    // Update is called once per frame
    void Update()
    {
        tailPos += (destTailPos - tailPos) * 0.5f;
        line.SetPosition(1, tailPos);
        if (ReleaseFlag == true)
        {
            Vector3 pos = transform.position;
            float dis = Mathf.Sqrt((pos.x - tailPos.x) * (pos.x - tailPos.x) + (pos.y - tailPos.y) * (pos.y - tailPos.y));
            if (dis < 0.1f)
            {
                Destroy(gameObject);
            }
        }
    }

   public  void SetPoint(LinePoint start, LinePoint end)
    {
        this.start = start;
        this.end = end;

        int index = start.GetId();
        endID = end.GetList().IndexOf(index);
        if (endID >= 0)
        {
            end.GetFlagList()[endID] = true;
        }
        index = end.GetId();
        startID = start.GetList().IndexOf(index);
        if (startID >= 0)
        {
            start.GetFlagList()[startID] = true;
        }
        destTailPos = end.transform.position;
    }

   public void Release()
    {
        destTailPos = transform.position;
        if (startID >= 0)
        {
			Debug.Log(startID);
            start.GetFlagList()[startID] = false;
        }
        if (endID >= 0)
        {
			Debug.Log(endID);
            end.GetFlagList()[endID] = false;
        }
        ReleaseFlag = true;

    }
}
