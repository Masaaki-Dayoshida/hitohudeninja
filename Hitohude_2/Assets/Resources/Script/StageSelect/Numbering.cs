﻿using UnityEngine;
using System.Collections;

public class Numbering : MonoBehaviour {

	public int headNum = 0;
	// Use this for initialization
	void Start()
	{

		number[] obj = GetComponentsInChildren<number>();
		int num = obj.GetLength(0);
		for (int cnt = 0; cnt < num; cnt++)
		{
			obj[cnt].SetNum(1 + cnt);
		}


	}

	// Update is called once per frame
	void Update()
	{

	}
}
