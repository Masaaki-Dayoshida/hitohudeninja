﻿using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {

	public Sprite image1;
	public Sprite image2;
	SpriteRenderer render;
	public int id = 0;
	private bool Enable = false;
	private bool Initialized = false;
	// Use this for initialization
	void Start()
	{
		render = gameObject.GetComponent<SpriteRenderer>();
		
	}
	void Init()
	{
		Initialized = true;
		int num = SavedataManager.ClearStageNum() + 1;
		if (id < num)
		{
			render.sprite = image1;
			Enable = true;
		}
		else
		{
			render.sprite = image2;
		}
	}
	// Update is called once per frame
	void Update () {
		if (!Initialized)
		{
			Init();
		}
	}

	void OnMouseDown()
	{
		
		render.sprite = image2;
	}

	void OnMouseUp()
	{
		if (Enable)
		{
			audio.Play();
			ListManager.SetStageNum(id);
			SceneChanger.ChangeScene("Game");
			AdBanner._Instance.Hide();
			render.sprite = image1;
		}
	}

	void OnMouseExit()
	{
		if (Enable)
		{
			render.sprite = image1;
		}
	}
	public void SetNum(int num)
	{
		id = num;
	}
	public void SetColor(Color color)
	{
		render.material.color = color;
	}
}
