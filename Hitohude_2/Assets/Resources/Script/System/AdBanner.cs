﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using GoodPlaceFramework;

public class AdBanner : MonoBehaviour
{
	// State Enum ==================================
	private enum INTERSTITIAL_STATE
	{
		STATE_NONE = 0,
		STATE_LOAD,
		STATE_DISP,
		STATE_COMPLETE,
		STATE_MAX
	};
	public static AdBanner _Instance;
	// local parameter =============================
	// private parameter
	private INTERSTITIAL_STATE State;
	static public BannerView banner;
	static public InterstitialAd interstitial;

	string banner_id = "";
	string interstitial_id = "";

	void Awake()
	{
        banner_id = GoodPlaceFramework.Server.GetBind("admob_banner_id");
        interstitial_id = GoodPlaceFramework.Server.GetBind("admob_inter_id");
        
        if (_Instance != null)
		{
			Destroy(gameObject);
			_Instance = null;
			return;
		}
		DontDestroyOnLoad(gameObject);
		_Instance = this;
		RequestBanner();
		GetComponent<Image>().enabled = false;
	}

	public
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		if (interstitial == null) return;
		if (State != INTERSTITIAL_STATE.STATE_NONE)
		{
			if (State == INTERSTITIAL_STATE.STATE_LOAD && interstitial.IsLoaded() == true)
			{
				Debug.Log("Interstitial Show");
				interstitial.Show();
				State = INTERSTITIAL_STATE.STATE_COMPLETE;
			}
		}

	}

	void RequestBanner()
	{
		banner = new BannerView(banner_id, AdSize.SmartBanner, AdPosition.Bottom);

		// 各イベントを格納
		banner.AdLoaded += HandleAdLoaded;
		banner.AdFailedToLoad += HandleAdFailedToLoad;
		banner.AdOpened += HandleAdOpened;
		banner.AdClosing += HandleAdClosing;
		banner.AdClosed += HandleAdClosed;
		banner.AdLeftApplication += HandleAdLeftApplication;

		// バナーをロード
		banner.LoadAd(createAdRequest());
	}

	public void Hide()
	{
#if UNITY_EDITOR
			GetComponent<Image>().enabled = false;
#endif
		if (banner != null)
		{
			RequestBanner();
		}
		banner.Hide();
	}

	public void Show()
	{
#if UNITY_EDITOR
			GetComponent<Image>().enabled = true;
#endif

		if (banner != null)
		{
			RequestBanner();
		}
		banner.Show();
	}


	//インタースティシャルのロード
	private AdRequest createAdRequest()
	{
		State = INTERSTITIAL_STATE.STATE_LOAD;

		return new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator)
			.AddKeyword("game").SetGender(Gender.Male)
			.SetBirthday(new System.DateTime(1985, 1, 1))
			.TagForChildDirectedTreatment(false)
			.AddExtra("color_bg", "9B30FF").Build();
	}
	void HandleAdLeftApplication(object sender, System.EventArgs e)
	{
		print("HandleInterstitialLeftApplication event received");
	}

	void HandleAdClosed(object sender, System.EventArgs e)
	{
		//Indicator.instance.isUse = false;
		print("HandleInterstitialClosed event received");
	}

	void HandleAdClosing(object sender, System.EventArgs e)
	{
		print("HandleInterstitialClosing event received");

	}

	void HandleAdOpened(object sender, System.EventArgs e)
	{
		print("HandleInterstitialOpened event received");
	}

	void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
	{
		//Indicator.instance.isUse = false;
		print("HandleInterstitialFailedToLoad event received with message: " + e.Message);
	}

	void HandleAdLoaded(object sender, System.EventArgs e)
	{
		print("HandleInterstitialLoaded event received.");
	}

	public void ShowInterstitial()
	{
		interstitial = new InterstitialAd(interstitial_id);

		interstitial.LoadAd(createAdRequest());
	}

	public void Apps()
	{
		Application.OpenURL("market://search?q=pub:good-place");
		
	}
	
	public void Review ()
	{
		Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw");
	}

}