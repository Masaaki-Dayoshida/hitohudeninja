﻿using UnityEngine;
using System.Collections;

public class DeathParent : MonoBehaviour {

    static private int count_ = 0;
    static private bool is_share = false;
    static public bool is_death_;

	// Use this for initialization
	void Start () {

        if (count_ == 0)
        {
            is_death_ = false;
        }
        else
        {
            is_death_ = true;

        }

        if (is_share == true)
        {
            count_++;
            if (count_ > 5)
            {
                count_ = 0;
                is_share = false;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(is_death_ == true)
        {
            Destroy(gameObject);
            is_share = true;

        }
	}

   static public void is_death()
    {
        count_++;
        is_death_ = true;
    }

}
