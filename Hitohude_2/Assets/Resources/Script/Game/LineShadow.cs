﻿using UnityEngine;
using System.Collections;

public class LineShadow : MonoBehaviour {

    LineRenderer line = null;
    Vector3 tailPos;
	// Use this for initialization
	void Start () {

        line = GetComponent<LineRenderer>();
        Vector3 pos = transform.position;
        line.SetPosition(0, pos);
        line.SetPosition(1, pos);
	}
	
	// Update is called once per frame
	void Update () {
        line.SetPosition(0, transform.position);
        line.SetPosition(1, tailPos);
	}

    public void SetLinePos(Vector3 start, Vector3 end)
    {
        if (line != null)
        {
            line.SetPosition(0, start);
            line.SetPosition(1, end);
            tailPos = end;
        }
    }
    public void SetColors(Color start, Color end)
    {
        if (line != null)
        {
            line.SetColors(start, end);
        }
    }
    public void SetTailPos(Vector3 pos)
    {
        tailPos = pos;
    }
}
