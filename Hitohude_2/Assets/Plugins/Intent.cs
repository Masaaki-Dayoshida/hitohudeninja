﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
//using GoodPlaceFramework;

#if UNITY_EDITOR
using UnityEditor;
#endif
public static class Intent{
	#if UNITY_EDITOR
	#elif UNITY_ANDROID
	static AndroidJavaObject m_plugin = null;
	#endif
#if UNITY_EDITOR
	static void share(string text, string url, string imagepath, string facebookMes="", string lineMes="") {
		Debug.Log(
			"text:"+text+System.Environment.NewLine+
			"url:"+url+System.Environment.NewLine+
			"path:"+imagepath+" "+System.IO.File.Exists(imagepath)+System.Environment.NewLine+
			"fb:"+facebookMes+System.Environment.NewLine+
			"line:"+lineMes
		);
    }
    static void onPostToFacebook(string text, string url, string imagePath)
    {
		Debug.Log("post to Facebook : "+text+","+url);
		SNSManager.instance.OnPostFacebook("OK");
	}
    static void onPostToTwitter(string text, string url, string textureUrl)
    {
        Debug.Log("post to Twitter : " + text + "," + url);
        SNSManager.instance.OnPostTwitter("OK");
	}
#elif UNITY_ANDROID
    static AndroidJavaObject unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    static AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

    // なんか色々出るｼｪｱ
	static void share(string text, string url, string imagepath, string facebookMes="", string lineMes="")
    {
		if ( m_plugin == null ){
			m_plugin = new AndroidJavaObject( "com.goodplace.adlibrary.IntentGP" );
		}
		if ( imagepath != "" ){

			m_plugin.Call ("Share", 
				text+" "+url,
				imagepath,
				facebookMes,
				lineMes);
		}else{
	        //instantiate the class Intent
	        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

	        //instantiate the object Intent
	        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

	        //call setAction setting ACTION_SEND as parameter
	        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

	        intentObject.Call<AndroidJavaObject>("putExtra", new object[] { "android.intent.extra.TEXT", ""+text+""+url });
	        //intentObject.Call<AndroidJavaObject>("putExtra", new object[] { "sms_body", ""+text+""+url });

	        //set the type of file
	        intentObject.Call<AndroidJavaObject>("setType", "text/plain");

	        //call the activity with our Intent
	        currentActivity.Call("startActivity", intentObject);

	        intentObject.Dispose();
		}
    }

	static void onPostToFacebook(string text, string url, string textureUrl) {
	Debug.Log("post to Facebook : "+text+","+url);
    	Debug.Log("post to Twitter : "+text+","+url);
        using (var intent = new AndroidJavaObject("android.content.Intent"))
        {
            intent.Call<AndroidJavaObject>("setAction", "android.intent.action.SEND");
            intent.Call<AndroidJavaObject>("setType", string.IsNullOrEmpty(textureUrl) ? "text/plain" : "image/png");
            intent.Call<AndroidJavaObject>("setPackage", "com.facebook.katana");

            if (!string.IsNullOrEmpty(url))
                text += "\t" + url;
            if (!string.IsNullOrEmpty(text))
                intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.TEXT", text);

            if (!string.IsNullOrEmpty(textureUrl))
            {
                var uri = new AndroidJavaClass("android.net.Uri");
                var file = new AndroidJavaObject("java.io.File", textureUrl);
                intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.STREAM", uri.CallStatic<AndroidJavaObject>("fromFile", file));
            }
            var chooser = intent.CallStatic<AndroidJavaObject>("createChooser", intent, "");
            chooser.Call<AndroidJavaObject>("putExtra", "android.intent.extra.EXTRA_INITIAL_INTENTS", intent);
            currentActivity.Call("startActivity", chooser);
        }
		SNSManager.instance.OnPostFacebook("OK");
	}

    private static void onPostToTwitter(string text, string url, string textureUrl)
    {
    	Debug.Log("post to Twitter : "+text+","+url);
        using (var intent = new AndroidJavaObject("android.content.Intent"))
        {
            intent.Call<AndroidJavaObject>("setAction", "android.intent.action.SEND");
            intent.Call<AndroidJavaObject>("setType", string.IsNullOrEmpty(textureUrl) ? "text/plain" : "image/png");
            intent.Call<AndroidJavaObject>("setPackage", "com.twitter.android");

            if (!string.IsNullOrEmpty(url))
                text += "\t" + url;
            if (!string.IsNullOrEmpty(text))
                intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.TEXT", text);

            if (!string.IsNullOrEmpty(textureUrl))
            {
                var uri = new AndroidJavaClass("android.net.Uri");
                var file = new AndroidJavaObject("java.io.File", textureUrl);
                intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.STREAM", uri.CallStatic<AndroidJavaObject>("fromFile", file));
            }
            var chooser = intent.CallStatic<AndroidJavaObject>("createChooser", intent, "");
            chooser.Call<AndroidJavaObject>("putExtra", "android.intent.extra.EXTRA_INITIAL_INTENTS", intent);
            currentActivity.Call("startActivity", chooser);
        }
		SNSManager.instance.OnPostTwitter("OK");
    }
#elif UNITY_IPHONE
    [DllImport("__Internal")]
	private static extern void share_(string text, string url, string imagePath);
	[DllImport("__Internal")]
	private static extern void onPostToFacebook_(string text, string url, string imagePath);
	[DllImport("__Internal")]
	private static extern void onPostToTwitter_(string text, string url, string imagePath);

	static void share(string text, string url, string imagepath, string facebookMes="", string lineMes=""){
	share_(text,url,imagepath);
    }
	static void onPostToFacebook(string text, string url, string imagePath) {
	onPostToFacebook_(text,url,imagePath);
	}
	static void onPostToTwitter(string text, string url, string imagePath) {
	onPostToTwitter_(text,url,imagePath);
	}
#endif
    public static void Share(string text, string url, string imagepath="", string facebookMes="", string lineMes="")
    {
		share(text,url,imagepath,facebookMes,lineMes);
    }
    public static void OnPostToFacebook(string text, string url, string imagePath)
	{
		SNSManager.isPostBoost = true;
        onPostToFacebook(text, url, imagePath);
	}
    public static void OnPostToTwitter(string text, string url, string textureUrl)
	{
		SNSManager.isPostBoost = true;
        onPostToTwitter(text, url, textureUrl);
	}
    //
}