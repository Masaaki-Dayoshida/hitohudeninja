﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RankUpEvent : MonoBehaviour {

	int currentRank = 0;
	int nextRank = 0;
	int Phase = 0;
	int frame = 0;
	RankManager rankManager = null;
	public Image Door_Left = null;
	public Image Door_Right = null;
	public Image Chapter = null;
	Color chapterColor;

	const float OpenPos_LeftX = -550.0f;
	const float OpenPos_RightX = 550.0f;
	const float ClosePos_LeftX = -177.0f;
	const float ClosePos_RightX = 177.0f;

	static string nextScene = "Adventure";
	static int sendNum = 0;
	float delta = 0;

	// Use this for initialization
	void Start () {
		currentRank = SavedataManager.Rank();
		int num = SavedataManager.ClearStageNum();
		nextRank = (num / 10) + 1;
		rankManager = FindObjectOfType<RankManager>();
		rankManager.SetRank(currentRank);
		
		Chapter.sprite = Resources.Load<Sprite>("Texture/Chapter/chapter" + nextRank);
		chapterColor = Chapter.color;

	}
	
	// Update is called once per frame
	void Update () {

		frame++;
		switch (Phase)
		{
			case 0:
				if (frame >= 60)
				{
					Phase++;
					frame = 0;
					delta = 0;
				}
				break;
			case 1:
				{
					float pos = leap(OpenPos_LeftX,ClosePos_LeftX, delta);
					Vector3 posL = Door_Left.transform.localPosition;
					posL.x = pos;
					Door_Left.transform.localPosition = posL;

					pos = leap(OpenPos_RightX,ClosePos_RightX, delta);
					Vector3 posR = Door_Right.transform.localPosition;
					posR.x = pos;
					Door_Right.transform.localPosition = posR;
					delta += 0.02f;
					if (delta >= 1.0f)
					{
						Phase++;
						frame = 0;
					}
				}
				break;
			case 2:
				rankManager.SetRank(nextRank);
				SavedataManager.SaveRank(nextRank);
				if (frame == 60)
				{
					frame = 0;
					Phase++;
					delta = 0;
				}
				break;
			case 3:
				{
					float pos = EaseIn(ClosePos_LeftX, OpenPos_LeftX, delta);
					Vector3 posL = Door_Left.transform.localPosition;
					posL.x = pos;
					Door_Left.transform.localPosition = posL;

					pos = EaseIn(ClosePos_RightX, OpenPos_RightX, delta);
					Vector3 posR = Door_Right.transform.localPosition;
					posR.x = pos;
					Door_Right.transform.localPosition = posR;
					delta += 0.01f;
					if (delta >= 1.0f)
					{
						Phase++;
						frame = 0;
					}
				}
				break;
			case 4:
				chapterColor.a += 0.02f;
				Chapter.color = chapterColor;
				Chapter.transform.Translate(new Vector3(0, 0.5f, 0));
				if (chapterColor.a >= 1.0f)
				{
					Destroy(Door_Left);
					Phase++;
					frame = 0;
				}
				break;
			default:
				break;

		}
	
	}

	public void NextScene()
	{
        SceneChanger.ChangeScene("Adventure");
		if (nextScene == "Game")
		{
			ListManager.SetStageNum(sendNum);
		}
		else if (nextScene == "StageSelect2")
		{
			ButtonManager.SetPade(sendNum);
		}
	}
	public static void SetNext(string next, int num)
	{
		nextScene = next;
		sendNum = num;
	}

	private float leap(float startPos, float endPos, float delta)
	{
		if (delta < 0)
		{
			delta = 0;
		}
		else if (delta > 1.0f)
		{
			delta = 1.0f;
		}
		return (startPos * (1.0f - delta) + endPos*(delta));
	}

	private float EaseIn(float startPos, float endPos, float delta)
	{
		if (delta < 0)
		{
			delta = 0;
		}
		else if (delta > 1.0f)
		{
			delta = 1.0f;
		}
		return -(startPos - endPos) * (delta * delta) + startPos;
	}

}
