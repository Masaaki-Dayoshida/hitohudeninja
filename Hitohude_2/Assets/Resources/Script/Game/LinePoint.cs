﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class LinePoint : MonoBehaviour {

    public int pointId;
    public Cursor Cursor;
    static int PointNum = 0;
	bool _Active = false;
    bool _Complete = false;
    List<int> ConnectList = new List<int>();
	List<bool> FlagList = new List<bool>();
	// Use this for initialization
	void Start () 
    {
        pointId = PointNum;
        PointNum++;
	}
    void Awake()
    {
        ConnectList.Clear();
        FlagList.Clear();
    }
    void OnDestroy()
    {
        PointNum--;
    }
	
	// Update is called once per frame
	void Update () 
    {
		if (_Active) 
		{
			gameObject.renderer.material.color = Color.red;	
		} 
		else 
		{
			gameObject.renderer.material.color = Color.white;
		}

        if (FlagList.Count > 0)
        {
            _Complete = (FlagList.IndexOf(false) < 0 ? true : false);
        }
        else
        {
            _Complete = false;
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
     
    }

    public int GetId()
    {
        return pointId;
    }

    public void AddConnect(int obj)
    {
        if (ConnectList.Count == 0 && FlagList.Count != 0)
        {
            FlagList.Clear();
        }
        ConnectList.Add(obj);
		FlagList.Add (true);
    }

	public List<int>GetList()
	{
		return ConnectList;
	}

	public List<bool>GetFlagList()
	{
		return FlagList;
	}

	public void SetActive(bool flag)
	{
		_Active = flag;
	}

    public void ClearnUp()
    {
        int index = FlagList.IndexOf(true);
        while (index >= 0)
        {
            FlagList.RemoveAt(index);
            index = FlagList.IndexOf(true);
        }
    }

	public bool Active() { return _Active; }
    public bool Complete() { return _Complete; }
}
