﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

	// このスクリプトの使い方 ==========================

	// ①
	// 各シーンのマネージャーオブジェクト、管理オブジェクトにコンポーネントする

	// ②
	// インスペクターに<FadeColor>,<WaitTime>,<FadeTime>,を設定
	// <FadeColor> : フェードの色
	// <FadeOutWaitTime> : フェードが始まるまでの時間
	// <FadeOutDelayTime> : フェードが終わるまでの時間 = 速度
	//  人人人
	// >注意！<
	//  Y^Y^Y
	// 0だと動作しないので、最低でも0.1を設定！

	// ③
	// シーン遷移したいときに、このスクリプトのStartFade()を呼び出す
	// <引数リストの説明>
	// <NextScene / string> : 次のシーンの名前

	// =================================================

	// public parameter
	public Color FadeColor = new Color(1.0f,1.0f,1.0f,1.0f);
	public float FadeOutWaitTime;
	public float FadeOutDelayTime;

	// private parameter
	private bool MoveScene;

	// static parameter 
	private static bool RequestMoveScene;
	private static string RequestNextSceneName = "";

	// Use this for initialization
	void Start () {
		if (FadeColor.a == 0.0f) 
		{
			FadeColor.a = 1;
		}
		RequestMoveScene = false;
		MoveScene = false;
		RequestNextSceneName = "";
	}
	
	// Update is called once per frame
	void Update () {

		if (RequestMoveScene == true && MoveScene == false )
		{
			CameraFade.StartAlphaFade(FadeColor, false, FadeOutWaitTime, FadeOutDelayTime, () => { Application.LoadLevel(RequestNextSceneName); });
			MoveScene = true;
		}
	}

	// onDestroy is called for this Object Destroiy 
	void onDestroy(){
	}

	public static void ChangeScene(string NextScene){
		RequestMoveScene = true;
		RequestNextSceneName = NextScene;
	}
}
