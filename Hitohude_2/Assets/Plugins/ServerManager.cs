﻿using UnityEngine;
using System.Collections;
using GoodPlaceFramework;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;

public class ServerManager : MonoBehaviour {
	public static ServerManager instance;
	WWW www;

    // 0 ... ダウンロード命令待ち.
	// 1 ... ダウンロード開始.
	public int status;

	// ファイル名.
	string filename;

	// 拡張子.
	string dataType;

	// フォーム用.
	string uid = "";
	string pass = "";
	string device = "";

	public string time = "";

	public float progress = 0f;
	public int splitcnt = 0;
	public GameObject Splash;
	public GameObject Systems;
	public GameObject GameContents;

	void Awake () {
		if (instance != null){
			Destroy(gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad(gameObject);

        status = 0;
		progress = 0f;
		uid = SystemInfo.deviceUniqueIdentifier;
		pass = EncryptRJ256(sKy,sIV,uid);
		device = SystemInfo.operatingSystem;

        //--  最初にサーバーからもらうデータを格納するフォルダの生成  --//
        Directory.CreateDirectory(Application.persistentDataPath + "/zip");
        Directory.CreateDirectory(Application.persistentDataPath + "/Texture");
        Directory.CreateDirectory(Application.persistentDataPath + "/Texture/Rank");
        Directory.CreateDirectory(Application.persistentDataPath + "/Texture/StageSelect");
        Directory.CreateDirectory(Application.persistentDataPath + "/Texture/StageSelect/PageTitle");

        App.Init();
		
        StartCoroutine(Init());

	}

	// ゲーム開始時に読み込みたいファイル.
	public IEnumerator Init(){
		Download(Server.AMS_DOWNLOAD,App.confFileName,"conf");
        while (status != 0) yield return null;

		// サーバからデータを取得.
		yield return StartCoroutine(ServerToLocal());
	}

    public IEnumerator AgencyDownload(string file)
    {
        Download(Server.APP_ROOT + "/" + file, file);
        while (status != 0) yield return null;

        // サーバからデータを取得.
        yield return StartCoroutine(ServerToLocal());
    }

	public float waitTimer = 0f;

	// ダウンロード処理と解凍処理.
	void Update () {
		if ( status == 0 ) return;

		if ( www.error != null ){
			Debug.Log("Failed : "+www.error);
			www.Dispose();
			status = 0;
			DialogManager.instance.ShowSubmitDialog (App.Rep(App.GetLang("server_error_title")), App.Rep(App.GetLang("server_error_mes")), (bool b) => {});
			if ( dataType == "conf" ){
				Server.SetBind();
				App.isInit = true;
				Systems.SetActive(true);
				GameContents.SetActive(true);
			}
			return;
		}

		progress = www.progress;
		waitTimer += Time.deltaTime;

		if ( Input.GetKey(KeyCode.Escape) ){
			Debug.Log("Failed : Connection Cancel");
			www.Dispose();
			status = 0;
			DialogManager.instance.ShowSubmitDialog (App.Rep(App.GetLang("server_error_title")), App.Rep(App.GetLang("server_error_mes")), (bool b) => {});
			return;
		}

		if ( www.isDone ){
			Debug.Log(App.Rep("Download Success : filename: "+filename+"%newline%waittime: "+waitTimer));
			if ( dataType == "conf" ){
				File.WriteAllText(Application.persistentDataPath+"/"+filename,www.text);
				Debug.Log(www.text);
				Server.SetBind();
				App.isInit = true;
				Application.LoadLevel("Title");
				
			}else if ( dataType == "txt" ){
				File.WriteAllText(Application.persistentDataPath+"/"+filename,www.text);
			}else if ( dataType == "zip" ){
				File.WriteAllBytes(Application.persistentDataPath+"/zip/"+filename,www.bytes);
				if ( splitcnt == 0 ){
					Zipper.UnZip(Application.persistentDataPath+"/zip/"+filename);
				}
			}else{
				File.WriteAllBytes(Application.persistentDataPath+"/"+filename,www.bytes);
			}
			www.Dispose();
            status = 0;
        }
	}

	// ローカルにデータがないならurl先に飛んで、name(拡張子つき).
	// を取得する.
	// typeの種類 ============.
	// txt.
	// conf.
	// mp3.
	// png.
	// zip.
	// split...zipファイルの分割数.
	// 使用中の場合はfalse.
	public float waitTime = 0;
	public bool Download(string url,string name,string type = "",int split=0){

		// 使用中.
		if ( status == 1 ) return false;

		// すでにダウンロードしているかどうか判定する.
		if ( type == "conf" ){
			// 設定ファイルは常時上書き.
		}else if ( type == "zip" ){
			// zipファイルは特殊なので場合分け.
			string dir = Application.persistentDataPath+"/"+name.Replace(".zip","");
			if ( Directory.Exists(dir) ){
				// ZIP解凍済みがあったら、すでにダウンロードしている判定をとる.
				//Debug.Log("Failed Download : Already Downloaded Directory"+System.Environment.NewLine+""+dir);
				status = 0;
				return true;
			
			}else if ( File.Exists(Application.persistentDataPath+"/zip/"+name) ){ 
				// まだ未解凍なら解凍する.
				//Debug.Log("Failed Download : Already Downloaded File");
				status = 0;
				Zipper.UnZip(dir+".zip");
				return true;
			}
		}else{
			if ( File.Exists(Application.persistentDataPath+"/"+name) ){
				//Debug.Log("Failed Download : Already Downloaded File");
				status = 0;
				return true;
			}
		}
		WWWForm form = new WWWForm();
		form.AddField("appname",App.name);
		if ( type == "zip" && split > 0 ){
			url = String.Format(url+"_{0}.downloadfile",""+split);
			name = String.Format(name+"_{0}.downloadfile",""+split);
			// ZIPの分割ファイルで、同じファイルをダウンロードしようとしていたらダウンロード判定.
			if ( File.Exists(Application.persistentDataPath+"/zip/"+name) ){
				status = 0;
				return true;
			}
		}
		form.AddField("filename",name);
		form.AddField("datatype",type);
		#if UNITY_ANDROID
		form.AddField("platform","Android");
		#elif UNITY_IPHONE
		form.AddField("platform","iOS");
		#endif
		//Debug.Log(url);
		www = new WWW(url,form);
		filename = name;
		dataType = type;
		waitTimer = 0f;
		status = 1;
		progress = 0f;
		splitcnt = split;
		return true;
	}
		
	// 残金チェック.
	public IEnumerator SubChip(Action<bool> callback, int price){
		yield return StartCoroutine(ServerToLocal());
		if ( User.chip >= price ){
			User.chip -= price;
			yield return StartCoroutine(LocalToServer());
			callback(true);
		}
		else callback(false);
	}

	// 仮想通貨同期 =======================================
	public static bool isLock = false;

	public IEnumerator LocalToServer(Action<bool> callback=null){
		yield return StartCoroutine(GetNowTime());
		if ( time == "" ) yield break;
		if ( isLock ){
			Debug.Log("同期がロックされています.");
			yield break;
		}
		isLock = true;

		string key = EncryptRJ256(sKy,sIV,uid+pass+time+"goodplace");
		WWWForm form = new WWWForm();
		form.AddField("uid",uid);
		form.AddField("pass",pass);
		form.AddField("time",time);
		form.AddField("key",key);
		form.AddField("device",device);
		form.AddField("chip",User.chip);


		using( WWW www = new WWW(Server.AMS_LOCAL_TO_SERVER,form) ){
			yield return www;

			if ( www.error != null ) {
				www.Dispose();
				status = 0;

				DialogManager.instance.ShowSubmitDialog (App.Rep(App.GetLang("server_error_title")), App.Rep(App.GetLang("server_error_mes")), (bool b) => {});
				isLock = false;
				if ( callback != null ){
					callback(false);
				}
				yield break;
			}
			Debug.Log(""+www.text);
		}
		isLock = false;
		if ( callback != null ){
			callback(true);
		}
	}

	public IEnumerator ServerToLocal(bool isShowRenewInfo = false){
		yield return StartCoroutine(GetNowTime());
		if ( time == "" ) yield break;
		if ( isLock ){
			Debug.Log("同期がロックされています.");
			yield break;
		}
		isLock = true;

		string key = EncryptRJ256(sKy,sIV,uid+pass+time+"goodplace");
		WWWForm form = new WWWForm();
		form.AddField("uid",uid);
		form.AddField("pass",pass);
		form.AddField("time",time);
		form.AddField("key",key);
		form.AddField("device",device);
		form.AddField("p","0");
		form.AddField("appc","0");

		using( WWW www = new WWW(Server.AMS_SERVER_TO_LOCAL,form) ){
			yield return www;

			if ( www.error != null ) {
				www.Dispose();
				status = 0;

				DialogManager.instance.ShowSubmitDialog (App.Rep(App.GetLang("server_error_title")), App.Rep(App.GetLang("server_error_mes")), (bool b) => {});
				isLock = false;
				yield break;
			}
			int oldchip = User.chip;
			Debug.Log("---before:"+oldchip);
			int.TryParse(www.text,out User.chip);
			int newchip = User.chip;
			Debug.Log("---after:"+newchip);
			if ( newchip > oldchip && isShowRenewInfo ){
				int chip = newchip - oldchip;
				DialogManager.instance.AddChip(0,chip+" "+App.GetLang("chip_get_mes"));
			}

		}
		isLock = false;
	}

	public IEnumerator GetNowTime ()
	{
		time = "";
		using( WWW www = new WWW(Server.AMS_TIME) ){
			yield return www;
			if ( www.error != null ){
				yield break;
			}
			time = www.text;
		}
	}

	public IEnumerator GetNowTimeFormat ()
	{
		time = "";
		using( WWW www = new WWW(Server.AMS_TIMEFORMAT) ){
			yield return www;
			if ( www.error != null ){
				yield break;
			}
			time = www.text;
		}
	}

	public IEnumerator Renew(){
		yield return StartCoroutine(ServerToLocal(true));
		yield return StartCoroutine(LocalToServer());
	}
	public IEnumerator RenewPopupReviewChip(){
		Application.OpenURL(Server.GetBind("popupReviewURL"));
		yield return StartCoroutine(ServerToLocal());
		int chip = int.Parse(Server.GetBind("popupReviewChip"));
		DialogManager.instance.AddChip(0,chip+" "+App.GetLang("chip_get_mes"));
	}
	// 暗号化. ===================================

	// KEY
	string sKy = "lkirwf897+22#bbtrm8814z5qq=498j5"; //'32 chr shared ascii string (32 * 8 = 256 bit)
	string sIV = "741952hheeyy66#cs!9hjv887mxx7@8y";  //'32 chr shared ascii string (32 * 8 = 256 bit)
	// 暗号化.
	public static string EncryptRJ256(string prm_key, string prm_iv, string prm_text_to_encrypt)
	{
		string sToEncrypt = prm_text_to_encrypt;

		RijndaelManaged myRijndael = new RijndaelManaged();
		myRijndael.Padding = PaddingMode.Zeros;
		myRijndael.Mode = CipherMode.CBC;
		myRijndael.KeySize = 256;
		myRijndael.BlockSize = 256;

		byte[] encrypted;
		byte[] toEncrypt;

		byte[] key = new byte[0];
		byte[] IV = new byte[0];

		key = System.Text.Encoding.UTF8.GetBytes(prm_key);
		IV = System.Text.Encoding.UTF8.GetBytes(prm_iv);

		ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);

		MemoryStream msEncrypt = new MemoryStream();
		CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

		toEncrypt = System.Text.Encoding.UTF8.GetBytes(sToEncrypt);

		csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
		csEncrypt.FlushFinalBlock();
		encrypted = msEncrypt.ToArray();

		return (Convert.ToBase64String(encrypted));
	}

    public static byte[] LoadByte(string file_name)
    {
        return File.ReadAllBytes(Application.persistentDataPath + "/" + file_name);
    }

    public static Sprite GetSprite(string path) {
        Rect rect;
        Texture2D tex;
        Sprite sprite = null;
        tex = new Texture2D(16, 16, TextureFormat.RGBA4444, false);
        tex.LoadImage(ServerManager.LoadByte(path));
        rect = new Rect(0, 0, tex.width, tex.height);
        sprite = Sprite.Create(tex, rect, new Vector2(0.5f, 0.5f));
        return sprite;
    }

    public static AudioClip GetAudio(string path) { 
        AudioClip audio;
        audio = AudioClip.Create(Application.persistentDataPath + "/" + path, 1, 1, 1, false, false);
        return audio;
    }

    public bool DownloadFile(string path)
    {
        //--  読み込むパス名を修正  --//
        string in_path = Server.APP_ROOT + "/" + path;
        string out_path = Application.persistentDataPath + "/" + path;

        //--  過去に読み込んだファイルはイラナイ  --//
        if (File.Exists(out_path))
        {return false;}

        WWWForm form = new WWWForm();
        form.AddField("appname", App.name);
        form.AddField("filename", path);
        form.AddField("datatype", "");

#if UNITY_ANDROID
        form.AddField("platform", "Android");
#elif UNITY_IPHONE
		form.AddField("platform","iOS");
#endif
        //--  サーバーからデータ取得  --//
        www = new WWW(in_path, form);

        //--  読み込みが終わるまで処理ストップ  --//
        while (!www.isDone){}

        //--  ローカルに格納  --//
        File.WriteAllBytes(out_path, www.bytes);
        www.Dispose();
        return true;
    }

    public bool DownloadFile(string path, string file)
    {
        //--  読み込むパス名を修正  --//
        string in_path = path;
        string out_path = Application.persistentDataPath + "/" + file;

        //--  過去に読み込んだファイルはイラナイ  --//
        if (File.Exists(out_path))
        { return false; }

        WWWForm form = new WWWForm();
        form.AddField("appname", App.name);
        form.AddField("filename", file);
        form.AddField("datatype", "");

#if UNITY_ANDROID
        form.AddField("platform", "Android");
#elif UNITY_IPHONE
		form.AddField("platform","iOS");
#endif
        //--  サーバーからデータ取得  --//
        www = new WWW(in_path, form);

        //--  読み込みが終わるまで処理ストップ  --//
        while (!www.isDone) { }

        //--  ローカルに格納  --//
        File.WriteAllBytes(out_path, www.bytes);
        www.Dispose();
        return true;
    }

    public static Sprite DownloadSprite(string path)
    {
        instance.DownloadFile(path);
        return GetSprite(path);
    }
}