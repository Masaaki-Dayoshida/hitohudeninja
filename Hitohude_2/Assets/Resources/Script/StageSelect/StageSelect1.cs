﻿using UnityEngine;
using System.Collections;

public class StageSelect1 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		AdBanner._Instance.Show();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Return();
		}
#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.C))
		{
			SavedataManager.Save(99);
			SavedataManager.SaveRank(10);
		}
#endif
	
	}

	public void SelectPage()
	{
		audio.Play();
		AdBanner._Instance.Hide();
		int num = SavedataManager.ClearStageNum();
		num = (num / 10) + 1;
		if (SavedataManager.Rank() < num)
		{
			RankUpEvent.SetNext("StageSelect2", PageManager.SelectPageNum());
			SceneChanger.ChangeScene("RankupEvent");
		}
		else
		{
			ButtonManager.SetPade(PageManager.SelectPageNum());
			SceneChanger.ChangeScene("StageSelect2");
		}
	}

	public void Return()
	{
		AdBanner._Instance.Hide();
		SceneChanger.ChangeScene("Title");
	}
}
