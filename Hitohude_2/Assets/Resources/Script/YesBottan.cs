﻿using UnityEngine;
using System.Collections;
using System.IO;

public class YesBottan : MonoBehaviour {

    public void OnClick()
    {
        if (BayWindow.is_show == BayWindow.SHOW_STATE.STATE_HINT)
        {
            BayWindow.is_bay = BayWindow.BAY_STATE.STATE_HINT_BAY;
        }
        else
        {
            BayWindow.is_bay = BayWindow.BAY_STATE.STATE_ANSWER_BAY;
        }
    }
}
