﻿using UnityEngine;
using System.Collections;

public class number : MonoBehaviour {

	public GameObject[] numberPlace;
	public int Num = 0;
	// Use this for initialization
	void Start () {
		int count = Num;
		Vector2 uv = new Vector2(0, 0);
		for (int cnt = 0; cnt < 2; cnt++)
		{
			uv.x = 0.1f * (count % 10);
			numberPlace[cnt].renderer.material.SetTextureOffset("_MainTex", uv);
			count /= 10;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SetNum(int num)
	{
		Num = num;
		int count = num;
		Vector2 uv = new Vector2(0, 0);
		for (int cnt = 0; cnt < 2; cnt++)
		{
			uv.x = 0.1f * (count % 10);
			numberPlace[cnt].renderer.material.SetTextureOffset("_MainTex", uv);
			count /= 10;
		}
	}
}
