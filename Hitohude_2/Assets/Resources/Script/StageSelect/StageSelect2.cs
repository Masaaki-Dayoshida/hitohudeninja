﻿using UnityEngine;
using System.Collections;

public class StageSelect2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AdBanner._Instance.Show();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Return();
		}
	
	}

	public void Return()
	{
		AdBanner._Instance.Hide();
		SceneChanger.ChangeScene("StageSelect1");
	}
}
