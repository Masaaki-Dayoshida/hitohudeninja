﻿using UnityEngine;
using System.Collections;

public class ButtonNumbering : MonoBehaviour {

	public int headNum = 0;
	// Use this for initialization
	void Start () {

		button[] obj = GetComponentsInChildren<button>();
		int num = obj.GetLength(0);
		for (int cnt = 0; cnt < num; cnt++)
		{
			obj[cnt].SetNum(headNum + cnt);
		}


	}

	// Update is called once per frame
	void Update()
	{
	
	}
}
