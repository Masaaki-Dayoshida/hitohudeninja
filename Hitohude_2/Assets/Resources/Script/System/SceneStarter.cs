﻿using UnityEngine;
using System.Collections;

public class SceneStarter : MonoBehaviour {

	// public parameter
	public float FadeInWaitTime;
	public float FadeInDelayTime;

	// Use this for initialization
	void Start () {
		CameraFade.StartAlphaFade(Color.black, true, FadeInWaitTime, FadeInDelayTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
