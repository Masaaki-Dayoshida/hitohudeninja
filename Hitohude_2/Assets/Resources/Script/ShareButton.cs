﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ShareButton : MonoBehaviour {
    public void OnTouch(){
        Debug.Log("OnTouch");
        File.WriteAllBytes(Application.persistentDataPath+"/screenshot.png",Resources.Load<Texture2D>("Texture/Title/title_bg").EncodeToPNG());
        Intent.Share(
            "○○ステージクリア!",
            "https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw",
            "" + Application.persistentDataPath + "/screenshot.png",
            "http://fl-game.com/spapp_gp/runyan/FB.php?filename=one_draw_screenshot",
            "line://msg/image/" + Application.persistentDataPath + "/screenshot.png"
        );
        //Intent.OnPostToTwitter(
        //    "○○ステージクリア!",
        //    "https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw",
        //    "" + Application.persistentDataPath + "/screenshot.png");
    }
    public void PointShare()
    {
        Debug.Log("PointShaer");
        File.WriteAllBytes(Application.persistentDataPath + "/screenshot.png", Resources.Load<Texture2D>("Texture/Title/title_bg").EncodeToPNG());
        Intent.Share(
            "○○ステージクリア!",
            "https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw",
            "" + Application.persistentDataPath + "/screenshot.png",
            "http://fl-game.com/spapp_gp/runyan/FB.php?filename=one_draw_screenshot",
            "line://msg/image/" + Application.persistentDataPath + "/screenshot.png"
        );

        SavedataManager.ValuePoint(100);
        DeathParent.is_death();
        
        //Intent.OnPostToTwitter(
        //    "○○ステージクリア!",
        //    "https://play.google.com/store/apps/details?id=jp.co.goodplace.onedraw",
        //    "" + Application.persistentDataPath + "/screenshot.png");
    }
}