﻿#if !UNITY_EDITOR
#define DEBUG_LOG_OVERWRAP
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;


// グローバルアプリ設定.
namespace GoodPlaceFramework{

    
	// 画像やオブジェクト等. =========================================
    public static class Util{

    	// 中央指定定数.
    	// 左上はVector2.up.
    	public static Vector2 center = new Vector2(0.5f,0.5f);
    	public static Texture2D tex;

		// 現在保存しているテクスチャのおもさ合計.
		public static decimal texAmountSize = 0;

		// キャッシュスプライト.
		static Dictionary<string,Sprite> sprites = new Dictionary<string,Sprite>();

		public static void Touch( string path ){
			if ( File.Exists( path ) == false ){
				File.Create( path ).Close();
			}
		}

        // オブジェクトに対して基本的な情報を設定して返す.
		public static GameObject CreateGameObject( string path, string name, Transform parent = null ){
            GameObject prefab = Resources.Load<GameObject>(path);
            if ( prefab == null ){
                Debug.Log("Not Found Prefab :"+path);
                return prefab;
            }
            GameObject obj = UnityEngine.Object.Instantiate(prefab) as GameObject ;
            obj.name = name;
			if ( parent != null ){
				obj.transform.parent = parent;
			}
            return obj;
        }

        // ローカルのテクスチャをスプライトに変換して返す.
		// file := Application.persistentDataPath以下.
		// pivot := 画像中心点 centerはUtil.centerで指定できる.
		// isOverWrite := falseで上書きしないになり、前回読み込んだスプライトを利用する.
		public static Sprite GetSprite( string file, Vector2 pivot, bool isOverWrite = false ){
			string key = file;
			if ( isOverWrite == false ){
	        	if ( sprites.ContainsKey(key) ){
	        		return sprites[key];
				}
			}

			string path = Application.persistentDataPath+"/"+file;

			#if UNITY_EDITOR
			tex = new Texture2D(16,16,TextureFormat.RGBA4444, false);
			#elif UNITY_ANDROID
			tex = new Texture2D(16,16,TextureFormat.ATC_RGBA8, false);
			#else
			tex = new Texture2D(16,16,TextureFormat.PVRTC_RGBA2, false);
			#endif
        	if ( !System.IO.File.Exists(path) ){
				Debug.Log("Failed GetSprite : not found file : "+path);
        		return null;
        	}

			tex.LoadImage(System.IO.File.ReadAllBytes(path));
        	tex.filterMode = FilterMode.Trilinear;

			if ( sprites.ContainsKey(key) ){
				sprites[key] = Sprite.Create (tex, new Rect (0f, 0f, tex.width, tex.height), pivot, 1f);
			}else{
				sprites.Add (key, Sprite.Create (tex, new Rect (0f, 0f, tex.width, tex.height), pivot, 1f));
				sprites[key].name = file;
			}

			#if UNITY_EDITOR
			texAmountSize += (decimal)4.0 * tex.width * tex.height;
			#elif UNITY_ANDROID
			texAmountSize += (decimal)tex.width * tex.height;
			#else
			texAmountSize += (decimal)0.25 * tex.width * tex.height;
			#endif
			return sprites[key];
        }
    }
    
	// ユーザ系データ.
	public static class User{
		// ユーザ設定データ.
		static Dictionary<string,string> savedata= new Dictionary<string,string>();

		// 仮想通貨.
		public static int chip = 0;

		// ユーザ設定ファイル.
		public static string confPath = "";
		public static string uid = "";
		public static string language = "";

		public static void Init(){
			#if UNITY_IPHONE
			iPhone.SetNoBackupFlag(Application.persistentDataPath);
			#endif
			QualitySettings.vSyncCount = 0; // VSyncをOFFにする
			Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定
			confPath = Application.persistentDataPath+"/savedata.txt";
			chip = 0;
			uid = SystemInfo.deviceUniqueIdentifier;
			language = Application.systemLanguage.ToString();
		}

		// 読み込み.
		public static void Load(){
			if ( File.Exists(confPath) == false ) File.WriteAllText(confPath,"");
			string conf = File.ReadAllText(confPath);
			savedata = new Dictionary<string,string>();

			string[] confs = conf.Replace("\r","").Split("\n"[0]);
			foreach ( string lines in confs ){
				string[] row = lines.Split(","[0]);
				string key = row[0];
				if ( key == "" ) continue;

				if ( row.Length < 2 ){
					Debug.Log("Failed SetBind length:"+key);
					continue;
				}
				
				if ( savedata.ContainsKey(key) ){
					Debug.Log("Failed SetBind duplicate:"+key);
				}else{
					savedata.Add(key,row[1]);
				}
			}
		}

		// 保存.
		public static void Save(){
			StringBuilder sb = new StringBuilder();
		    foreach (KeyValuePair<string,string>pair in savedata){
				sb.AppendLine(pair.Key + "," + pair.Value);
		    }
			File.WriteAllText(confPath,sb.ToString());
		}

		// 設定.
		public static void SetData(string key, string val){
			if ( savedata.ContainsKey(key) ){
				savedata[key] = val;
			}else{
				savedata.Add(key,val);
			}
		}

		// 取得.
		public static string GetData(string key){
			if ( savedata.ContainsKey(key) ){
				return savedata[key];
			}else{
				Debug.Log("Failed GetData : "+ key);
			}
			return "";
		}

		public static Dictionary<string,string> CheckData(){
			return savedata;
		}
	}
	
	// アプリ系データ.
	public static class App{

		// 各アプリ毎にプロジェクト名(ローマ字)を変えること!
        public static string name = "hitohudeNinja";

		// アプリ表示名.
        public static string displayName = "ひと筆;";

		// バンドルID.
        public static string bundleid = "jp.co.goodplace.hitohudeNinja";

		// アプリ設定ファイル.
		public static string confPath = "";
        public static string confFileName = "hitohudeNinja.txt";

		public static bool isPostBoost = false;

		// デバッグ.
		public static bool isDebug = false;

		// 初期化.
		public static bool isInit = false;
		// 終了.
		public static bool isFinish = false;

		// ゲームデータ置換.
		public static string Rep(string str){
			str = str.Replace("%newline%", System.Environment.NewLine );
			str = str.Replace("%stacktrace%", System.Environment.StackTrace );
			str = str.Replace("%conma%", "," );
			str = str.Replace("%chip%", ""+User.chip );
			/*foreach ( KeyValuePair<string,string> pair in User.CheckData() ){
				BigDecimal tmp = 0;
				if ( BigDecimal.TryParse( pair.Value, out tmp ) ){
					str = str.Replace("%"+pair.Key+"%", ((BigInteger)tmp).ToUnitString() );
				}else{
					str = str.Replace("%"+pair.Key+"%", pair.Value );
				}
			}*/
			return str;
		}


		// ローカライズ.
		static Dictionary<string,string> lang;
		public static string LANGUAGE_ROOT = "la_JP";

		public static void Init(){
			confPath = Application.persistentDataPath+"/"+confFileName;
			Util.texAmountSize = 0;
			User.Init();
			SetLang();
		}
			
		public static void SetLang(){
			if ( User.language == "Japanese" ){
				LANGUAGE_ROOT = "la_JP";
			}else{
				LANGUAGE_ROOT = "la_EN";
			}
			string conf = Resources.Load<TextAsset>(LANGUAGE_ROOT+"/"+LANGUAGE_ROOT).text;
			lang = new Dictionary<string,string>();

			string[] confs = conf.Replace("\r","").Split("\n"[0]);
			foreach ( string lines in confs ){
				string[] row = lines.Split(","[0]);
				string key = row[0];

				if ( key == "" ) continue;
				lang.Add(key,row[1]);
			}
		}

		public static string GetLang(string key){
			if ( lang.ContainsKey(key) ){
				return lang[key];
			}else{
				Debug.Log("Failed Get Lang:"+key);
				return "";
			}
		}

	}

	// サーバー系データ.
	public static class Server{
		// アプリ設定データ.
		static Dictionary<string,string> bind;

		// サーバ自体のルート.
		public static string SERVER_ROOT = "http://fl-game.com/spapp_gp";

		// アップロード.
		public static string AMS_UPLOAD = "http://fl-game.com/spapp_gp/upload.php";
		
		// ダウンロード.
		public static string AMS_DOWNLOAD = "http://fl-game.com/spapp_gp/download.php";
		
		// 時間取得.
		public static string AMS_TIME = "http://fl-game.com/spapp_gp/time.php";

		// 形式化された時間取得.
		public static string AMS_TIMEFORMAT = "http://fl-game.com/spapp_gp/time_format.php";

		// アプリルートパス.
        public static string APP_ROOT = "http://fl-game.com/spapp_gp/hitohudeNinja";

		// 仮想通貨同期用.
		public static string AMS_LOCAL_TO_SERVER = APP_ROOT+"/local_to_server.php";
		public static string AMS_SERVER_TO_LOCAL = APP_ROOT+"/server_to_local.php";

        // アクツールのKey値を設定
        public static void SetBind()
        {
			if ( File.Exists(App.confPath) == false ) File.WriteAllText(App.confPath,"");
			string conf = File.ReadAllText(App.confPath);
			bind = new Dictionary<string,string>();

			string[] confs = conf.Replace("\r","").Split("\n"[0]);
			foreach ( string lines in confs ){
				string[] row = lines.Split(","[0]);
				string key = row[0];

				if ( key == "" ) continue;

				if ( row.Length < 2 ){
					Debug.Log("Failed SetBind length:"+key);
					continue;
				}
				
				if ( bind.ContainsKey(key) ){
					Debug.Log("Failed SetBind duplicate:"+key);
				}else{
					bind.Add(key,row[1]);
				}
			}
		}

        // アクツールで設定したKey値の取得
		public static string GetBind(string key){
			if ( bind.ContainsKey(key) ){
				return bind[key];
			}
			Debug.Log("Failed GetBind : "+ key);
			return "";
		}
	}

}
	
